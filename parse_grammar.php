<?php
$a = file_get_contents("parser_cpp_language.y");
$arList = explode("\n", $a);
$arResultList = array();
foreach($arList as $string)
{
	if (mb_strpos($string, "echo_if_debug") === false)
	{
		$arResultList[]  = $string;
	}
}
$a = implode("\n", $arResultList);
$arMatches = array();

// Split to rules
preg_match_all("/[a-zA-Z0-9_]+\\([a-zA-Z0-9_]+\\)[ ]*::=[^.]*\\./", $a, $arMatches);

// Make list of rules
global $arRules;
$arRules = array();
foreach($arMatches[0] as $arMatch)
{
	$data = preg_replace("/\\([^)]+\\)/", "", $arMatch);
	$data = str_replace(".", "", $data);
	$ruleParts = array_map('trim', explode("::=", $data));
	$rightRuleParts = array_map('trim', explode(' ', $ruleParts[1]));
	if (count($rightRuleParts)) {
		$rightRuleParts = array_filter($rightRuleParts, function($o) { return mb_strlen($o) != 0; });
	}
	if ((!in_array('comment_list', $rightRuleParts)) 
		&& ($ruleParts[0] != "comment_list") &&
		(!in_array('preprocessor_if', $rightRuleParts)) 
		&& ($ruleParts[0] != "preprocessor_if") &&
		(!in_array('preprocessor_concat', $rightRuleParts)) 
		&& ($ruleParts[0] != "preprocessor_concat") &&
		(!in_array('preprocessor_stringify', $rightRuleParts)) 
		&& ($ruleParts[0] != "preprocessor_stringify") &&
		(!in_array('macro', $rightRuleParts)) 
		&& ($ruleParts[0] != "macro")) {
		$arRules[] = array(
			'left' => $ruleParts[0],
			'right' => $rightRuleParts
		);
	}
}

//var_dump($arRules);

$arRules = array_values($arRules);

function get_definitions_for_symbol($sym)
{
	global $arRules;
	$arResult = array();
	foreach($arRules as $rule) {
		if ($rule['left'] == $sym) {
			$arResult[] = $rule['right'];
		}
	}
	return $arResult;
}

function is_terminal($sym)
{
	return count(get_definitions_for_symbol($sym)) == 0;
}

global $arTerminalDefinitions;
$arTerminalDefinitions = array(
	'COMMENT' => array('/* test */'),
	'PREPROCESSOR_STRINGIFY' => array('##'),
	'TYPENAME' => array(
		'QPointF',
		'QLineF',
		'Vector',
		'Window',
	),
	'DOT' => array('.'),
	'LESSER' => array('<'),
	'GREATER' => array('>'),
	'NAMESPACE_RESOLVE' => array('::'),
	'CONSTKWD' => array('const'),
	'COMMA' => array(','),
	'PREPROCESSOR_CONCAT' => array('#'),
	'MULTIPLY' => array('*'),
	'AMPERSAND' => array('&'),
	'VOLATILEKWD' => array('volatile'),
	'LEFTROUNDBRACKET' => array('('),
	'RIGHTROUNDBRACKET' => array(')'),
	'SIGNED' => array('signed'),
	'LONG' => array('long'),
	'INT' => array('int'),
	'DOUBLE' => array('double'),
	'SHORT' => array('short'),
	'UNSIGNED' => array('unsigned'),
	'CHAR' => array('char'),
	'FLOAT' => array('float'),
	'VOID' => array('void'),
	'IDENTIFIER' => array(
		'width',
		'height',
		'unit',
		'window'
	),
	'INLINEKWD' => array('inline'),
	'REGISTERKWD' => array('register'),
	'VIRTUALKWD' => array('virtual'),
	'FRIENDKWD' => array('friend'),
	'STRUCTKWD' => array('struct'),
	'UNIONKWD' => array('union'),
	'CLASSKWD' => array('class'),
	'LEFTSQUAREBRACKET' => array('['),
	'RIGHTSQUAREBRACKET' => array(']'),
	'SIZEOF' => array('sizeof'),
	'EXTERNKWD' => array('extern'),
	'STRING' => array(
		"\"Option\"",
		"\"Test\""
	),
	'CHARACTER' => array(
		"'A'",
		"'B'",
		"'C'",
		"'D'",
		"'E'",
		"'F'",
		"'G'",
		"'H'",
		"'I'",
		"'J'",
		"'K'",
		"'L'",
		"'M'",
		"'N'",
		"'O'",
		"'P'",
		"'Q'",
		"'R'",
		"'S'",
		"'T'",
		"'U'",
		"'V'",
		"'W'",
		"'X'",
		"'Y'",
		"'Z'",
	),
	'NEWKWD' => array('new'),
	'STATICKWD' => array('static'),
	'EQUAL' => array('equal'),
	'LOGICALNOT' => array('!'),
	'LOGICALOR' => array('||'),
	'NUMERIC' => array(
		'0',
		'1',
		'2',
		'3',
		'4',
		'5',
		'6',
		'7',
		'8',
		'9',
		'10',
		'-1',
		'42',
		'1689',
		'2504'
	),
	'PLUS' => array('+'),
	'DECREMENT' => array('--'),
	'INCREMENT' => array('++'),
	'MUTABLEKWD' => array('mutable'),
	'MINUS' => array('-'),
	'CONST_CAST' => array('const_cast'),
	'STATIC_CAST' => array('static_cast'),
	'ASSIGN' => array('='),
	'GREATER_OR_EQUAL' => array('>='),
	'LESSER_OR_EQUAL' => array('<='),
	'LEFTFIGUREBRACKET' => array('{'),
	'RIGHTFIGUREBRACKET' => array('}'),
	'BINARYXOR' => array('^'),
	'BINARYOR' => array('|'),
	'BINARYAND' => array('&'),
	'BINARYNOT' => array('~'),
	'OPERATOROVERLOADDECLARATION' => array('operator+',  'operator-', 'operator*', 'operator/', 'operator<', 'operator>', 'operator==', 'operator!='),
	'RIGHTARROW' => array('->'),
	'LEFTSHIFT' => array('<<'),
	'RIGHTSHIFT' => array('>>'),
	'BINARYOR_ASSIGN' => array('|='),
	'DIVISION' => array('/'),
	'REINTERPRET_CAST' => array('reinterpret_cast'),
	'DYNAMIC_CAST' => array('dynamic_cast'),
	'NOT_EQUAL' => array('!='),
	'LOGICALAND' => array('&'),
	'MULTIPLY_ASSIGN' => array('*='),
	'LEFTSHIFT_ASSIGN' => array('<<='),
	'RIGHTSHIFT_ASSIGN' => array('>>='),
	'COLON' => array(':'),
	'SEMICOLON' => array(';'),
	'MODULOSIGN' => array('%'),
	'PLUS_ASSIGN' => array('+='),
	'MODULO_ASSIGN' => array('%='),
	'BINARYAND_ASSIGN' => array('&='),
	'QUESTION' => array('?'),
	'DIVISION_ASSIGN' => array('/='),
	'PUBLICKWD' => array('public'),
	'MINUS_ASSIGN' => array('-='),
	'BINARYXOR_ASSIGN' => array('^='),
	'DELETE' => array('delete'),
	'TEMPLATEKWD' => array('template'),
	'ENUMKWD' => array('enum'),
	'PRIVATEKWD' => array('private'),
	'WHILEKWD' => array('while'),
	'BREAKKWD' => array('break'),
	'RETURNKWD' => array('return'),
	'TYPEDEF' => array('typedef'),
	'TRYKWD' => array('try'),
	'CONTINUEKWD' => array('continue'),
	'CATCHKWD' => array('catch'),
	'ELLIPSIS' => array('...'),
	'TYPENAMEKWD' => array('typename'),
	'PROTECTEDKWD' => array('protected'),
	'DOKWD' => array('do'),
	'SWITCHKWD' => array('switch'),
	'IFKWD' => array('if'),
	'ELSEKWD' => array('else'),
	'FORKWD' => array('for'),
	'GOTOKWD' => array('goto'),
	'SIGNALKWD' => array('signal'),
	'DEFAULTKWD' => array('default')
);

function start_with($string, $substr)
{
	return mb_substr($string, 0, mb_strlen($substr)) == $substr;
}

function produce_tree($sym, $args, $i = 0)
{
	global $arTerminalDefinitions;
	$arRules = get_definitions_for_symbol($sym);
	if ($args['had_expr_on_top']) {
		$arNewRules = array();
		// Удаляем дурацкие варианты генерации выражения, делаем более осмысленными
		foreach($arRules as $rule) {
			if (!in_array('namespace_resolve', $rule) 
				&& !in_array('namespace_resolve_from_start', $rule)  
				&& !in_array('scoped_type', $rule) 
				&& !in_array('scoped_identifier', $rule)
				&& !in_array('operatoroverloaddeclaration', $rule)
				&& !in_array('decl_specifiers', $rule)
				&& !in_array('friendkwd', $rule)
				&& !in_array('volatilekwd', $rule)
				&& !in_array('virtualkwd', $rule)
				&& !in_array('constkwd', $rule)
				&& !in_array('externkwd', $rule)
				&& !in_array('registerkwd', $rule)
				&& !in_array('cv_qualifier', $rule)
				&& !in_array('typename', $rule)
				&& !in_array('typename', $rule)
				&& !in_array('typename_or_instantiated_template_type', $rule)
				&& !in_array('instantiated_template_type', $rule)
				&& !in_array('template_instantiation_arguments_begin', $rule)
				&& !in_array('abstract_declarator', $rule)
				&& !in_array('abstract_declarator_ref', $rule)
				&& !in_array('abstract_declarator_ptrs', $rule)) {
				$arNewRules[] = $rule;
			}
		}
		if (($args['expr_level'] > 5) && ($sym == 'expr')) {
			$arNewRules = array(array('NUMERIC'), array('IDENTIFIER'));
		}
		if ($sym == 'customtype') {
			$arNewRules = array(array('TYPENAME'));
		}
		$arRules = $arNewRules;
	}
	if (($i > 20) && (count($arRules) > 0)) 
	{
		return $sym;
	}
	if (count($arRules)) {
		$rule = $arRules[rand(0, count($arRules) - 1)];
		$result = array();
		$localArgs = $args;
		if ($sym == 'expr') {
			$localArgs['had_expr_on_top'] = true;
		}
		if ($localArgs['had_expr_on_top']) {
			$localArgs['expr_level'] += 1;
		}
		if (count($rule)) {
			foreach($rule as $localSym) {
				$result[] = produce_tree($localSym, $localArgs, $i + 1);
			}
		}
		return implode(' ', $result);
	} else {
		if (array_key_exists($sym, $arTerminalDefinitions)) {
			$arDefinitions =  $arTerminalDefinitions[$sym];
			return $arDefinitions[rand(0, count($arDefinitions) - 1)];
		} else {
			echo "No definitions for symbol: " . $sym . "\n";
			return $sym;
		}
	}
}

srand(time(null));
$args = array(
	'had_expr_on_top' => false,
	'expr_level' => 0
);
echo produce_tree('expr', $args);