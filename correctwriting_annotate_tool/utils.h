/*! \file utils.h
 *  An utility functions
 */
#pragma once
#include <QRectF>
#include <QPointF>
#include <QString>
#include <QStringList>
#include <qline.h>

/*! A maybe value
 */
template<
    typename T
>
struct Maybe
{
    bool exists;  //!< Whether value exists
    T value;      //!< A value

    /*! Constructs new empty maybe
     */
    inline Maybe() : exists(false) { } 
    /*! Constructs new non-empty maybe
     *  \param[in] v constructs a value
     */
    inline Maybe(const T& v) : exists(true), value(v) {};

    inline bool operator==(const T& v) const
    {
        return exists && (value == v);
    }
};

/*! Tests whether point is within rectangle
    \param[in] p a point
    \param[in] r rectangle
    \return whether point is within rectangle
 */
bool isWithin(const QPointF& p, const QRectF& r);
/*! Returns whole collision point from line and rectangle
 *  \param[in] l line
 *  \param[in] r rectang;e
 *  \return a collision point
 */
Maybe<QPointF> collisionPoint(const QLineF& l, const QRectF& r);
 

inline QString toString(unsigned int a)
{
    return QString::number(a);
}

inline QString toString(double a)
{
    return QString::number(a);
}

template<typename T>
inline QString toString(const Maybe<T>& a)
{
    if (a.exists)
    {
        QString t("true|");
        t.append(toString(a.value));
        return t;
    }
    return "false";
}

inline QString toString(const QPointF& a)
{
    return toString(a.x()) + ";" + toString(a.y());
}

inline QString toString(const QRectF& r)
{
    return toString(r.top()) + ";" + toString(r.left()) + ";" + toString(r.width()) + ";" + toString(r.height());
}

/*! Tries to convert from string to uint
 */
inline Maybe<unsigned int> toUint(const QString& s)
{
    bool ok = true;
    unsigned int result = s.toUInt(&ok);
    if (ok)
    {
        return {result};
    }
    return Maybe<unsigned int>();
}

/*! Tries to convert from string to uint
 */
inline Maybe<double> toDouble(const QString& s)
{
    bool ok = true;
    double result = s.toDouble(&ok);
    if (ok)
    {
        return {result};
    }
    return Maybe<double>();
}

inline void parse(const QString& s, Maybe<unsigned int>& r)
{
    r = toUint(s);
}


inline void parse(const QString& s, Maybe<double>& r)
{
    r = toDouble(s);
}

inline void parse(const QString& s, Maybe<QPointF>&  point)
{
    Maybe<double> p1;
    Maybe<double> p2;
    point = Maybe<QPointF>();
    QStringList list = s.split(";");
    if (list.size() == 2)
    {
        parse(list[0], p1);
        parse(list[1], p2);
        if (p1.exists && p2.exists)
        {
            point = {QPointF(p1.value, p2.value)};
        }
    }
}

inline void parse(const QString& s, Maybe<QRectF>&  point)
{
    Maybe<double> p1;
    Maybe<double> p2;
    Maybe<double> p3;
    Maybe<double> p4;
    point = Maybe<QRectF>();
    QStringList list = s.split(";");
    if (list.size() == 4)
    {
        parse(list[0], p1);
        parse(list[1], p2);
        parse(list[2], p3);
        parse(list[3], p4);

        if (p1.exists && p2.exists  && p3.exists  && p4.exists)
        {
            point = {QRectF(p1.value, p2.value, p3.value, p4.value)};
        }
    }
}




template<typename T>
inline void parse(const QString& s, Maybe<Maybe<T> >& a)
{
    QStringList list = s.split("|");
    a = {};
    if (list.size() > 0)
    {
        if (list.size() == 1)
        {
            if (list[0] == "false")
            {
                a = {Maybe<T>()};
            }
        }
        else
        {
            if (list[0] == "false")
            {
                a = {Maybe<T>()};
            }
            else
            {
                Maybe<T> tmp;
                parse(list[1], tmp);
                if (tmp.exists)
                {
                    a = {tmp};
                }
            }
        }
    }
}

