#include "node.h"
#include "settings.h"
#include "utils.h"

#include <QFont>
#include <QTextDocument>
#include <QFontMetricsF>
#include <QPainter>

#define OFFSET 10
Node::Node() : Id(0)
{
    setText("Node");
}

void Node::setText(const QString& text)
{
    m_text = text;

    // Recompute bounding rect
    QFontMetricsF metrics(QFont(FONT, FONT_SIZE));
    double height = 2;
    double width = 0;
    QStringList lst = m_text.split("\n", QString::KeepEmptyParts);
    for(int i = 0; i < lst.size(); i++)
    {
        if (i != 0)
        {
            height += OFFSET;
        }
        QSizeF size = metrics.size(0, lst[i]);
        width = std::max(size.width(), width);
        height += size.height();
    }
    m_rect = QRectF(0, 0, width, height);
}

const QString& Node::text() const
{
    return m_text;
}

QDomElement Node::toElement(QDomDocument& doc)
{
    QDomElement e = doc.createElement("Node");
    e.setAttribute("Id", toString(this->Id));
    e.setAttribute("Text", this->text());
    e.setAttribute("Pos", toString(this->pos()));
    return e;
}

bool Node::loadFromElement(QDomElement& e)
{
    Maybe<unsigned int> id;
    QString text = e.attribute("Text");
    Maybe<QPointF> pos;
    if (text.length() == 0)
    {
        return false;
    }
    parse(e.attribute("Id"), id);
    parse(e.attribute("Pos"), pos);
    if (id.exists && pos.exists)
    {
        this->Id = id.value;
        this->setText(text);
        this->setPos(pos.value);
        return true;
    }
    return false;
}

Node* Node::clone() const
{
    Node* node = new Node();
    node->setText(this->text());
    node->setPos(this->pos());
    node->Id = this->Id;
    return node;
}

QRectF Node::boundingRect() const
{
    return m_rect;
}

void Node::paint(QPainter* p, const QStyleOptionGraphicsItem*, QWidget*)
{
    QFont old_font = p->font();
    QFont font(FONT, FONT_SIZE);
    p->setFont(font);
    QBrush black(QColor(0, 0, 0));
    p->setPen(QPen(black, 1));

    QStringList lst = m_text.split("\n", QString::KeepEmptyParts);
    double y = 0;
    QFontMetricsF metrics(QFont(FONT, FONT_SIZE));
    for(int i = 0; i < lst.size(); i++)
    {
        if (i != 0)
        {
            y += OFFSET;
        }
        QSizeF size = metrics.size(0, lst[i]);
        p->drawText(QPointF(m_rect.width() / 2 - size.width() / 2, y + size.height()), lst[i]);
        y += size.height();
    }
    //p->drawRect(m_rect);
    p->setFont(old_font);
}
