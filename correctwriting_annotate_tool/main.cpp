#include "mainwindow.h"
#include <QtWidgets/QApplication>
#include <QVector>


QVector<MainWindow *> AdditionalWindows;

int main(int argc, char *argv[])
{
    QStringList paths = QCoreApplication::libraryPaths();
    paths.append(".");
    paths.append("imageformats");
    paths.append("platforms");
    paths.append("sqldrivers");
    QCoreApplication::setLibraryPaths(paths);

    QApplication a(argc, argv);

    MainWindow w(&a);
    w.setMinimumSize(w.size());
    w.setMaximumSize(w.size());
    w.show();
    QObject::connect(&a, SIGNAL(lastWindowClosed()), &w, SLOT(storeConfigToFile()));
    int result =  a.exec();
    for(int i = 0; i < AdditionalWindows.length(); i++)
    {
        delete AdditionalWindows[i];
    }
    return result;
}
