/*! A main scene object
 */
#pragma once
#include <QGraphicsScene>
#include <QVector>
#include <QSet>
#include "node.h"
#include "arrow.h"
#include "annotationrule.h"
#include "annotationline.h"

class MainWindow;

class Scene: public QGraphicsScene
{
Q_OBJECT
public:
    struct NodeChange  // NOLINT
    {
        Node* Node;            // A current starting node

        QString OldText;       // An old text
        QString NewText;       // A new text

        QRectF OldPosition;    // An old position
        QRectF NewPosition;    // A new position

        QVector<Arrow*> ArrowsComingFromNode;
        QVector<QPointF> ArrowsComingFromNodeOldPositions;
        QVector<QPointF> ArrowsComingFromNodeNewPositions;

        QVector<Arrow*> ArrowsComingToNode;
        QVector<QPointF> ArrowsComingToNodeOldPositions;
        QVector<QPointF> ArrowsComingToNodeNewPositions;


        QVector<AnnotationLine*> LinesComingFromNode;
        QVector<QPointF> LinesComingFromNodeOldPositions;
        QVector<QPointF> LinesComingFromNodeNewPositions;
    };
    struct AnnotationRuleChange // NOLINT
    {
        AnnotationRule* Rule;  // An annotation rule

        QString OldText; // An old text
        QString NewText; // A new text

        QRectF OldRect; // An old position
        QRectF NewRect; // An old position

        Maybe<QRectF> OldMaxRect;
        Maybe<QRectF> NewMaxRect;

        QVector<AnnotationLine*> Lines;
        QVector<QPointF> OldPositions;
        QVector<QPointF> NewPositions;

        ::Scene::AnnotationRuleChange& operator=(const AnnotationRuleChange& annotation_rule_change);
    };
    enum State
    {
        S_IDLE,
        S_MOVING_NODE,
        S_MOVING_ARROW,
        S_ADDING_NODE,

        S_REMOVING,

        S_ARROW_PLACE_FIRST_POINT,
        S_ARROW_PLACE_SECOND_POINT,

        S_ADDING_RULE,
        S_MOVING_RULE,

        S_RESIZING_RULE_LEFT_SIDE,
        S_RESIZING_RULE_RIGHT_SIDE,
        S_RESIZING_RULE_TOP_SIDE,
        S_RESIZING_RULE_BOTTOM_SIDE,

        S_RESIZING_RULE_TOP_LEFT_CORNER,
        S_RESIZING_RULE_TOP_RIGHT_CORNER,
        S_RESIZING_RULE_BOTTOM_LEFT_CORNER,
        S_RESIZING_RULE_BOTTOM_RIGHT_CORNER,

        S_ANNOTATION_LINE_PLACE_FIRST_POINT,
        S_ANNOTATION_LINE_PLACE_SECOND_POINT,

        S_MOVING_ANNOTATION_LINE
    };
    /*! Makes new scene
     */
    Scene(QObject *parent = 0);
    /*! Returns new id for object
     *  \return id
     */
    unsigned int getId();
    /*! Adds node to list of nodes
     *  \param[in] node a node
     */
    void addNode(Node* node);
    /*! Adds arrow to list of arrows
     *  \param[in] arrow an arrow
     */
    void addArrow(Arrow* arrow);
    /*! Adds an annotation rule
     *  \param[in] rule a rule
     */
    void addAnnotationRule(AnnotationRule* rule);
    /*! Adds an annotation line
     *  \param[in] line an annotation line
     */
    void addAnnotationLine(AnnotationLine* line);
    /*! Clones new scene to new scene
        \return cloned scene
     */
    Scene* clone() const;
    /*! Sets window for scene
     *  \param[in] win window
     */
    void setWindow(MainWindow* win);
    /*! A reaction for mouse press event
     *  \param e event
     */
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *e);
    /*! A reaction for mouse release event
     *  \param e event
     */
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *e);
    /*! A reaction for mouse move event
     *  \param e event
     */
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *e);
    /*! Enables moving the arrows
     */
    void enableArrowMoving();
    /*! Disable moving the arrows
     */
    void disableArrowMoving();
    /*! Hides node, removing it from storage
     *  \param[in] node a node
     */
    void hideAndRemoveNode(Node* node);
    /*! Shows and adds node
     *  \param[in] node a node
     */
    void showAndAddNode(Node* node);
    /*! Hides arrow, removing it from storage
     *  \param[in] arrow an arrow
     */
    void hideAndRemoveArrow(Arrow* arrow);
    /*! Shows and adds node
     *  \param[in] arrow an arrow
     */
    void showAndAddArrow(Arrow* arrow);
    /*! Hides and removes annotation rule
     *  \param[in] rule a rule
     */
    void hideAndRemoveAnnotationRule(AnnotationRule* rule);
    /*! Shows and adds annotation rule
     *  \param[in] rule a rule
     */
    void showAndAddAnnotationRule(AnnotationRule* rule);
    /*! Hides and removes annotation rule
     *  \param[in] line a line
     */
    void hideAndRemoveAnnotationLine(AnnotationLine* line);
    /*! Shows and adds annotation rule
     *  \param[in] line a line
     */
    void showAndAddAnnotationLine(AnnotationLine* line);
    /*!
     * Starts adding node
     * \param[in] node node name
     */
    void startAddingNode(const QString& node);
    /*!
     * A signal, that text inside of window is edited
     * \param[in] text a text
     */
    void textEdited(const QString& text);
    /*! An action, that is performed by pressing Esc
     */
    void escapeAction();
    /*! If node is selected, changes text
     * \param[in] node a node
     * \param[in] text a text
     */
    void tryChangeTextIfNodeIsSelected(Node* node, const QString& text) const;
    /*! Starts removing items from scene
     */
    void startRemoving();
    /*! Startes adding arrow
     */
    void startAddingArrow();
    /*!
     * Starts adding node
     * \param[in] node node name
     */
    void startAddingAnnotationRule(const QString& node);
    /*! Starts adding annotation line
     */
    void startAddngAnnotationLine();
    /*! Returns an element, that stores an annotation rule
     *  \param[in] doc document
     *  \return element
     */
    QDomElement toElement(QDomDocument& doc);
    /*! Loads item from element
     *  \param[in] element
     *  \return result
     */
    bool loadFromElement(QDomElement& e);
    /*! Cleans a scene
     */
    void cleanup();
    /*! Expands scene
     */
    void expand();
private:
    /*! A maxiumal id to be stored in scene
     */
    unsigned int m_max_id;
    /*! A list of nodes
     */
    QHash<unsigned int, Node*> m_nodes;
    /*! A list of arrows
     */
    QHash<unsigned int, Arrow*> m_arrows;
    /*! A list of annotation rules
     */
    QHash<unsigned int, AnnotationRule*> m_annotation_rules;
    /*! A list of annotation lines
     */
    QHash<unsigned int, AnnotationLine*> m_annotation_lines;

    /*! Whether user can move arrows
     */
    bool m_can_move_arrows;
    /*! A window, stored inside
     */
    MainWindow* m_main_window;

    Node* m_current_node;   // A current moving node
    Scene::NodeChange m_change; // An old change


    QPointF m_start_pos;    // A start position
    QPointF m_clicked_pos;  // A clicked position


    Arrow* m_current_arrow; // A current moving arrow
    bool m_first; // Whether first point of arrow is moving

    AnnotationRule* m_current_rule; // A current rule
    AnnotationRuleChange m_rule_change; // A rule change

    AnnotationLine* m_current_line; // A current line
    QLineF m_old_line; // An old line

    QVector<AnnotationLine*> m_attached_lines; // An attached lines

    bool m_last_clicked_on_node; // Whether we clicked last time on node
    /*!  A resizing states for rules
     */
    QSet<Scene::State> m_rule_resizing_states;

    Scene::State m_state;          // A state
    Scene::State m_possible_state; // A possible state


    void moveNodeTo(const QPointF& p);
    void moveArrowTo(const QPointF& p) const;
    void moveAnnotationRuleTo(const QPointF& p);
    void moveAnnotationLineTo(const QPointF& p) const;
    /*! Returns list of arrows coming from node
     *  \param[in] node anode
     *  \return a list of arrows
     */
    QVector<Arrow*> getArrowsCommingFromNode(Node* node);
    /*! Returns list of arrows coming from node
     *  \param[in] node anode
     *  \return a list of arrows
     */
    QVector<Arrow*> getArrowsCommingToNode(Node* node);
    /*! Returns first points for arrows
     *  \param arrows list of arrows
     *  \return list
     */
    static QVector<QPointF> getFirstPointsForArrows(const QVector<Arrow*>& arrows);
    /*! Returns second points for arrows
     *  \param arrows list of arrows
     *  \return list
     */
    static QVector<QPointF> getLastPointsForArrows(const QVector<Arrow*>& arrows);
    /*! Returns list of lines attached to rule
     *  \param[in] node a node
     *  \return list of lines
     */
    QVector<AnnotationLine*> getLinesAttachedToNode(Node* node);
    /*! Returns list of lines attached to rule
     *  \param[in] rule a rule
     *  \return list of lines
     */
    QVector<AnnotationLine*> getLinesAttachedToRule(AnnotationRule* rule);
    /*! Returns list of first points for lines
     *  \param[in] lines list of lines
     *  \return list of points
     */
    static QVector<QPointF> getFirstPointsForLines(const QVector<AnnotationLine*>& lines);
    /*! Returns list of last points for lines
     *  \param[in] lines list of lines
     *  \return list of points
     */
    static QVector<QPointF> getLastPointsForLines(const QVector<AnnotationLine*>& lines);
    /*! Saves moving resizing state for rule
     *  \param[in] rule a rule
     *  \param[in] p point
     */
    void saveMovingResizingStateForRule(AnnotationRule* rule, const QPointF& p);
    /*! Tests, whether user clicked on rule corner, changing m_possible_state
     *  \param[in] pos a position, where user has clicked
     *  \return whether he clicked on rule corners
     */
    bool isClickedOnRuleCorners(const QPointF& pos);
    /*! Makes scene go to rule corner resizing state
     *  \param[in] pos a position
     */
    void goIntoRuleCornerResizeState(const QPointF& pos);
    /*! Tests, whether user clicked on rule sides, changing m_possible_state
     *  \param[in] pos a position, where user has clicked
     *  \return whether he clicked on rule sides
     */
    bool isClickedOnRuleSides(const QPointF& pos);
    /*! Makes scene go to rule side resizing state
     *  \param[in] pos a position
     */
    void goIntoRuleSideResizeState(const QPointF& pos);
    /*! Tests, whether user clicked on rule, changing m_possible_state
     *  \param[in] pos a position, where user has clicked
     *  \return whether he clicked on rule
     */
    bool isClickedOnRule(const QPointF& pos);
    /*! Makes scene go to rule moving state
     *  \param[in] pos a pos
     */
    void goIntoRuleMovingState(const QPointF& pos);
    /*! Tests, whether user clicked on arrow, changing m_possible_state
     *  \param[in] pos position
     *  \return a pos
     */
    bool isClickedOnArrow(const QPointF& pos);
    /*! Makes scene go to state, when arrow point is changing
     */
    void goIntoArrowChangingPointState();
    /*! Tests, whether user clicked on node, changing m_possible_state
     *  \param[in] pos a position
     *  \return  whether user clicked on node
     */
    bool isClickedOnNode(const QPointF& pos);
    /*! Makes scene transition to moving state
     * \param[in] pos a position
     */
    void goToNodeMovingState(const QPointF& pos);
    /*! Tests, whether user clicked on annotation line, changing m_possible_state
     *  \param[in] pos position
     *  \return a pos
     */
    bool isClickedOnAnnotationLine(const QPointF& pos);
    /*! Makes scene go to state, when arrow point is changing
     */
    void goIntoAnnotationLineChangingPointState();
    /*! Stores old node info
     *  \param[in] node a node
     *  \return old node information
     */
    Scene::NodeChange storeOldNodeInfo(Node* node);
    /*! Stores old rule info
     *  \param[in] rule a rule
     *  \return a change
     */
    Scene::AnnotationRuleChange storeOldRuleInfo(AnnotationRule* rule);
    /*! Propagates  changes in rectangle to arrows and lines for node
     *  \param[in] change a changed data info
     *  \param[in] node a node
     */
    void propagateChangesInRectangleToArrowsAndLinesForNode(Scene::NodeChange& change, Node* node) const;
    /*! Propagates changes in rectangle to lines
     *  \param[in] change a change
     *  \param[in] rule a new rule
     */
    void propagateChangesInRectangleToLines(Scene::AnnotationRuleChange& change, AnnotationRule* rule) const;
    /*! Resizes rule according to state
     *  \param[in] pos position
     */
    void resizeRuleAccordingToState(const QPointF& pos);
    /*! Expands scene rect if needed
     *  \param[in] item an item
     */
    void expandSceneRect(QGraphicsItem* item = NULL);
};
