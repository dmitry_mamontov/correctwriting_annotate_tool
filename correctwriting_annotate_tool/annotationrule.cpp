#include "annotationrule.h"
#include "settings.h"

#include <QGraphicsScene>
#include <QPen>
#include <QBrush>
#include <QPainter>
#include <QFontMetricsF>

#define RULE_FONT_SIZE 

AnnotationRule::AnnotationRule() : Id(0)
{
    
}

void AnnotationRule::setText(const QString& text)
{
    m_text = text;
    recompute();
}

const QString& AnnotationRule::text() const
{
    return m_text;
}

bool AnnotationRule::isSetMaximalWidth() const
{
    return m_maximal_rect.exists;
}

Maybe<QRectF> AnnotationRule::maximalRect() const
{
    return m_maximal_rect;
}

void AnnotationRule::setMaximalRect(const Maybe<QRectF>& rectangle)
{
    m_maximal_rect = rectangle;
    if (m_maximal_rect.exists)
    {
        m_current_rect = m_maximal_rect.value;
    }
    recompute();
}


AnnotationRule* AnnotationRule::clone() const
{
    AnnotationRule* node = new AnnotationRule();
    node->setText(this->text());
    node->setPos(this->pos());
    node->Id = this->Id;
    node->m_current_rect = m_current_rect;
    node->m_maximal_rect = m_maximal_rect;
    return node;
}

QDomElement AnnotationRule::toElement(QDomDocument& doc)
{
    QDomElement e = doc.createElement("AnnotationRule");
    e.setAttribute("Id", toString(this->Id));
    e.setAttribute("Text", this->text());
    e.setAttribute("Pos", toString(this->pos()));
    e.setAttribute("CurrentRect", toString(m_current_rect));
    e.setAttribute("MaximalRect", toString(m_maximal_rect));
    return e;
}

bool AnnotationRule::loadFromElement(QDomElement& e)
{
    Maybe<unsigned int> id;
    QString text = e.attribute("Text");
    Maybe<QPointF> pos;
    Maybe<Maybe<QRectF> > max_rect;
    Maybe<QRectF> cur_rect;
    if (text.length() == 0)
    {
        return false;
    }
    parse(e.attribute("Id"), id);
    parse(e.attribute("Pos"), pos);
    parse(e.attribute("CurrentRect"), cur_rect);
    parse(e.attribute("MaximalRect"), max_rect);

    if (id.exists && pos.exists && cur_rect.exists && max_rect.exists)
    {
        this->Id = id.value;
        this->setText(text);
        this->setMaximalRect(max_rect.value);
        this->m_current_rect = cur_rect.value;
        this->setPos(pos.value);
        return true;
    }
    return false;
}

QRectF AnnotationRule::boundingRect() const
{
    return {0, 0, m_current_rect.width(), m_current_rect.height()};
}

void AnnotationRule::paint(QPainter* p, const QStyleOptionGraphicsItem*, QWidget*)
{
    QFont oldfont = this->scene()->font();
    p->setFont(QFont("Times New Roman", ANNOTATION_FONT_SIZE));

    QBrush red(QColor(255, 0, 0));

    QBrush brush(QColor(255, 128, 128, 128));
    p->setPen(QPen(red, 1));
    
    p->fillRect(this->boundingRect(), brush);
    p->drawRect(this->boundingRect());

    QBrush black(QColor(0, 0, 0));
    p->setPen(QPen(black, 1));
    p->drawText(2, 0, m_current_rect.width() - 4, m_current_rect.height(), Qt::AlignLeft | Qt::AlignTop | Qt::TextWordWrap, m_text);

    p->setFont(oldfont);
}

void AnnotationRule::recompute()
{
    if (!m_maximal_rect.exists)
    {
        QFont font("Times New Roman", ANNOTATION_FONT_SIZE);
        QFontMetricsF metrics(font);
        QRectF rect = metrics.boundingRect(QRectF(0, 0, 500, 10000), Qt::AlignLeft | Qt::AlignTop | Qt::TextWordWrap, m_text);
        m_current_rect = QRectF(m_current_rect.left(), m_current_rect.top(), rect.width() + 6, rect.height() + 8);
    }
}

