/*! \file arrow.h
 *   
 *   An arrow, that will be used to render some arrows in editor
 */
#pragma once
#include "utils.h"
#include <QGraphicsLineItem>
#include <QDomElement>

class Arrow: public QGraphicsLineItem
{
public:
    /*! An arrow
     */
    Arrow();
    /*! A from node id
     */
    Maybe<unsigned int> FromId; 
    /*! A to node id
     */
    Maybe<unsigned int> ToId;
    /*! A local id
     */
    unsigned int Id;

    /*! Returns an element, that stores an arrow
     *  \param[in] doc document
     *  \return element
     */
    QDomElement toElement(QDomDocument& doc);
    /*! Loads item from element
     *  \param[in] element
     *  \return result
     */
    bool loadFromElement(QDomElement& e);
    /*! Clones an arrow
        \return a cloned arrow
     */
    Arrow* clone() const;
protected:
    /*! Paints an annotation
     */
    virtual void paint(QPainter* p, const QStyleOptionGraphicsItem* option, QWidget* widget);
};

