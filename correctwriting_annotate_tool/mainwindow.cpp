#include "mainwindow.h"

// ReSharper disable once CppUnusedIncludeDirective
#include <QFileDialog>
#include <QMessageBox>
#include <QProcess>
#include <QTimer>

#include <QJsonParseError>
#include <QJsonObject>

#include <QFile>
#include <QFileInfo>
#include <QTextStream>

#include <QGraphicsSimpleTextItem>

#include <QFileDialog>
#include <QDomDocument>

#include "arrow.h"
#include "utils.h"
#include "node.h"

/*! A macro definition folder for config data, that specifies path to folder
 */
#define NKPO_DISTR_FOLDER "nkpo_distr_folder"
/*! A default nkpo distr path
 */
#define DEFAULT_NKPO_DIST_FOLDER "C:/Projects/CorrectWriting/nkpo_distr_v8/"
/*! A config file name
 */
#define CONFIG_FILE_NAME "config.json"
/*! A layout padding for subtree
 */
#define LAYOUT_PADDING_X 10
/*! An Y layout padding for subtree
 */
#define LAYOUT_PADDING_Y 50

// External storage for all new windows
extern QVector<MainWindow *> AdditionalWindows;


//  ================================= PUBLIC METHODS =================================

MainWindow::MainWindow(QApplication* app, QWidget *parent)  // NOLINT
    : QMainWindow(parent), m_source_scene(NULL), m_destination_scene(NULL), m_history_cursor_position(0), m_app(app)
{
    ui.setupUi(this);

    ui.txtEdit->setWordWrapMode(QTextOption::WrapAnywhere);

    m_source_scene = new Scene(this);
    m_source_scene->setWindow(this);
    m_source_scene->disableArrowMoving();

    m_destination_scene = new Scene(this);
    m_destination_scene->setWindow(this);

    ui.grpSourceTree->setScene(m_source_scene);
    m_source_scene->setSceneRect(0, 0, ui.grpSourceTree->width() - 2, ui.grpSourceTree->height() - 2);
    ui.grpTransformedTree->setScene(m_destination_scene);
    m_destination_scene->setSceneRect(0, 0, ui.grpTransformedTree->width() - 2, ui.grpTransformedTree->height() - 2);

    ui.grpSourceTree->setMouseTracking(true);
    ui.grpSourceTree->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);

    ui.grpTransformedTree->setMouseTracking(true);
    ui.grpTransformedTree->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);

    m_parser_path = DEFAULT_NKPO_DIST_FOLDER;

    tryLoadConfigFromFile();

    connect(ui.actionSpecify_path, SIGNAL(triggered(bool)), this, SLOT(specifyFolderForParser(bool)));
    connect(ui.btnParse, SIGNAL(clicked()), this, SLOT(tryParse()));

    connect(ui.btnUndo, SIGNAL(clicked()), this, SLOT(undo()));
    connect(ui.btnRedo, SIGNAL(clicked()), this, SLOT(redo()));

    connect(ui.txtEdit, SIGNAL(textChanged()), this, SLOT(innerTextChanged()));
    connect(ui.btnAddNode, SIGNAL(clicked()), this, SLOT(startAddingNode()));
    connect(ui.btnRemove, SIGNAL(clicked()), this, SLOT(startRemoving()));
    connect(ui.btnAddEdge, SIGNAL(clicked()), this, SLOT(startAddingArrow()));
    connect(ui.btnAddAnnotation, SIGNAL(clicked()), this, SLOT(startAddingAnnotationRule()));
    connect(ui.btnConnectAWN, SIGNAL(clicked()), this, SLOT(startAddingAnnotationLine()));

    connect(ui.actionNew, SIGNAL(triggered()), this, SLOT(newFile()));
    connect(ui.action_Exit, SIGNAL(triggered()), m_app, SLOT(quit()));
    connect(ui.actionS_ave_as, SIGNAL(triggered()), this, SLOT(saveAs()));
    connect(ui.action_Save, SIGNAL(triggered()), this, SLOT(save()));
    connect(ui.action_Open, SIGNAL(triggered()), this, SLOT(load()));
    connect(ui.actionNewWindow, SIGNAL(triggered()), this, SLOT(newWindow()));
}

MainWindow::~MainWindow()
{
    for(int i = m_history_items.size() - 1; i > -1; i--)
    {
        delete m_history_items[i];
    }
}

void MainWindow::tryLoadConfigFromFile()
{
    QString file_path = QCoreApplication::applicationDirPath();
    file_path.append("/").append(CONFIG_FILE_NAME);

    QFileInfo fi(file_path);
    if (fi.isFile() && fi.exists())
    {
        QFile file(file_path);
        if (file.open(QIODevice::ReadOnly)) 
        {
            {
                QString string;

                QTextStream stream(&file);
                string.append(stream.readAll());

                QJsonParseError error;  // NOLINT
                error.offset = 0;
                error.error = QJsonParseError::NoError;
                QJsonDocument document = QJsonDocument::fromJson(string.toUtf8(), &error);
                if (error.error == QJsonParseError::NoError)
                {
                    if (document.isObject())
                    {
                        QJsonObject object = document.object();
                        if (object.contains(NKPO_DISTR_FOLDER))
                        {
                            QJsonValue val = object[NKPO_DISTR_FOLDER];
                            if (val.isString())
                            {
                                m_parser_path = val.toString();
                            }
                            else
                            {
                                QMessageBox::critical(NULL, "Unable to load config", QString("Unable to parse config \"") + file_path + QString("\", cannot load path"));
                            }
                        }
                    }
                    else
                    {
                        QMessageBox::critical(NULL, "Unable to load config", QString("Unable to parse config \"") + file_path + QString("\", top unit is not object"));
                    }
                }
                else
                {
                    QMessageBox::critical(NULL, "Unable to load config", QString("Unable to parse config \"") + file_path + QString("\": ") + error.errorString());
                }
            }
            file.close();
        }
        else
        {
            QMessageBox::critical(NULL, "Unable to load config", QString("Unable to load config \"") + file_path + QString("\""));
        }
    }
}

void MainWindow::tryRunParser(const QString& string)
{
    QString current_path = QDir::currentPath();
    if (!QDir::setCurrent(m_parser_path))
    {
        QMessageBox::critical(NULL, "Critical failure", QString("Cannot set current folder to ") + m_parser_path);
    }

    if (writeParsedString(string))
    {
        QProcess p;
        p.start("cmd.exe", QStringList() << "/C" <<"run.bat");
        p.waitForFinished();
        int exit_code = p.exitCode();
        if (exit_code == 0)
        {
            bool ok = true;
            QString parsed_data = this->tryReadParsedFile(&ok);
            if (ok)
            {
                if (parsed_data.startsWith('<'))
                {
                    QDomDocument document;
                    if (document.setContent(parsed_data))
                    {
                        Scene* new_source = new Scene();
                        new_source->setWindow(this);
                        new_source->disableArrowMoving();

                        Scene* new_dest = new Scene();
                        new_dest->setWindow(this);

                        Scene* old_source = this->m_source_scene;
                        Scene* old_dest = this->m_destination_scene;
                        this->m_source_scene = new_source;
                        this->m_destination_scene = new_dest;
                        ui.grpSourceTree->setScene(new_source);
                        ui.grpTransformedTree->setScene(new_dest);
                        QHash<MainWindow::XMLNode*, ::Node*> storage;
                        MainWindow::XMLNode* xml_node = this->parse(document.documentElement(), storage, this->m_source_scene);
                        QTimer::singleShot(500, [=] {
                            layoutNodes(LAYOUT_PADDING_X, -LAYOUT_PADDING_Y + 10, xml_node, storage);
                            recomputeSceneBounds(this->m_source_scene, storage);
                            this->copyToDestination();
                            delete xml_node;
                        });

                        MainWindow::NewParseItem* item  = new MainWindow::NewParseItem();
                        item->NewSourceScene = new_source;
                        item->NewDestinationScene = new_dest;
                        item->OldSourceScene = old_source;
                        item->OldDestinationScene = old_dest;
                        item->OwnedScene1 = old_source;
                        item->OwnedScene2 = old_dest;
                        item->Window = this;
                        addHistoryItem(item);
                    }
                    else
                    {
                        QMessageBox::critical(NULL, "Critical failure", "Unable to parse output XML data");
                    }
                }
                else
                {

                    QMessageBox::critical(NULL, "Critical failure", parsed_data.toLocal8Bit());
                }
            }
            else
            {
                QMessageBox::critical(NULL, "Critical failure", "Cannot read output file");
            }
        }
        else
        {
            QMessageBox::critical(NULL, "Critical failure", QString("Executable exited with code: ")  + QString::number(exit_code));
        }
    }
    else
    {
        QMessageBox::critical(NULL, "Critical failure", "Cannot write input file");
    }


    if (!QDir::setCurrent(current_path))
    {
        QMessageBox::critical(NULL, "Critical failure", QString("Cannot set current folder to ") + current_path);
    }

}

void MainWindow::setScenes(Scene* source, Scene* dest)
{
    this->m_source_scene = source;
    this->m_destination_scene = dest;
    ui.grpSourceTree->setScene(source);
    ui.grpTransformedTree->setScene(dest);
}

void MainWindow::addHistoryItem(MainWindow::HistoryItem* item)
{
    for(int i = m_history_items.size() - 1; i >= m_history_cursor_position; i--)
    {
        delete m_history_items[i];
        m_history_items.removeAt(i);
    }
    m_history_items << item;
    ++m_history_cursor_position;
}

void MainWindow::setDestinationScene(Scene* source)
{
    this->m_destination_scene = source;
    ui.grpTransformedTree->setScene(source);
}

void MainWindow::setStatusLabel(const QString& s) const
{
    ui.lblStatus->setText(s);
}

void MainWindow::keyPressEvent(QKeyEvent * e)
{
    this->QMainWindow::keyPressEvent(e);
    if (e->key() == Qt::Key_Escape)
    {
        this->m_destination_scene->escapeAction();
    }
    if (e->key() == Qt::Key_S && (e->modifiers() & Qt::ControlModifier))
    {
        this->save();
    }
    if (e->key() == Qt::Key_O && (e->modifiers() & Qt::ControlModifier))
    {
        this->load();
    }
    if (e->key() == Qt::Key_N && (e->modifiers() & Qt::ControlModifier))
    {
        this->newFile();
    }
    if (e->key() == Qt::Key_Z && (e->modifiers() & Qt::ControlModifier))
    {
        if (!(this->ui.txtEdit->hasFocus()) && !(this->ui.txtExpression->hasFocus()))
        {
            this->undo();
        }
    }
    if (e->key() == Qt::Key_Y && (e->modifiers() & Qt::ControlModifier))
    {
        if (!(this->ui.txtEdit->hasFocus()) && !(this->ui.txtExpression->hasFocus()))
        {
            this->redo();
        }
    }

    if (e->key() == Qt::Key_Z && (e->modifiers() & Qt::AltModifier))
    {
        this->startRemoving();
    }
    if (e->key() == Qt::Key_Tab && (e->modifiers() & Qt::ControlModifier))
    {
        this->tryParse();
    }
}

void MainWindow::setEditableTextValue(const QString& name) const
{
    ui.txtEdit->blockSignals(true);
    ui.txtEdit->setPlainText(name);
    ui.txtEdit->blockSignals(false);
}

void MainWindow::tryChangeTextIfNodeIsSelected(Node* node, const QString& text) const
{
    this->m_destination_scene->tryChangeTextIfNodeIsSelected(node, text);
}


//  ================================= PUBLIC SLOTS =================================

void MainWindow::specifyFolderForParser(bool)
{
   QString result = QFileDialog::getExistingDirectory(this, "Specify path to nkpo_distr for parsing data (nkpo_distr_v8 for example)", "Specify path for parser");
    if (result.length())
    {
        if (result.right(1) != '/')
        {
            result.append('/');
        }
        m_parser_path = result;
    }
}


void MainWindow::storeConfigToFile() const
{
    QJsonDocument document;
    QJsonObject object;
    object.insert(NKPO_DISTR_FOLDER, m_parser_path);
    document.setObject(object);

    QString file_path = QCoreApplication::applicationDirPath();
    file_path.append("/").append(CONFIG_FILE_NAME);

    QFile file(file_path);
    if (file.open(QIODevice::WriteOnly))
    {
        {
            QTextStream stream(&file);
            stream << document.toJson();
        }
        file.close();
    }
    else
    {
        QMessageBox::critical(NULL, "Unable to save config", QString("Unable to save config\"") + file_path + QString("\""));
    }
}

void MainWindow::tryParse()
{
    QString expression = ui.txtExpression->toPlainText().trimmed();
    if (expression.length() != 0)
    {
        tryRunParser(expression);
    }
}

void MainWindow::copyToDestination()
{
    Scene* scene = this->m_source_scene->clone();
    Scene* old_destination = this->m_destination_scene;

    setDestinationScene(scene);

    CopyItem* item = new CopyItem();
    item->NewDestinationScene = scene;
    item->OldDestinationScene = old_destination;
    item->OwnedScene = old_destination;
    item->Window = this;

    addHistoryItem(item);
}

void MainWindow::undo()
{
    if (m_history_cursor_position > 0)
    {
        m_history_items[m_history_cursor_position - 1]->undo();
        m_history_cursor_position--;
    }
}

void MainWindow::redo()
{
    if (m_history_cursor_position < m_history_items.size())
    {
        m_history_items[m_history_cursor_position]->redo();
        m_history_cursor_position++;
    }
}

void MainWindow::innerTextChanged() const
{
    QString s = ui.txtEdit->toPlainText();
    if (s.contains("\n") || s.contains("\r"))
    {
        s = s.replace("\n", "").replace("\r", "");
        QTextCursor cursor = ui.txtEdit->textCursor();
        ui.txtEdit->blockSignals(true);
        ui.txtEdit->setPlainText(s);
        ui.txtEdit->blockSignals(false);
        ui.txtEdit->setTextCursor(cursor);
    } 
    else
    {
        m_destination_scene->textEdited(ui.txtEdit->toPlainText());
    }
}

void MainWindow::startAddingNode() const
{
    m_destination_scene->startAddingNode(ui.txtEdit->toPlainText());
}

void MainWindow::startRemoving() const
{
    m_destination_scene->startRemoving();
}

void MainWindow::startAddingArrow() const
{
     m_destination_scene->startAddingArrow();
}


void MainWindow::startAddingAnnotationRule() const
{
    m_destination_scene->startAddingAnnotationRule(ui.txtEdit->toPlainText());
}

void MainWindow::startAddingAnnotationLine() const
{
    m_destination_scene->startAddngAnnotationLine();
}

void MainWindow::disableEditing() const
{
    ui.txtEdit->setDisabled(true);
}

void MainWindow::enableEditing() const
{
    ui.txtEdit->setDisabled(false);
}

void MainWindow::newFile()
{
    Scene* old_source_scene = m_source_scene;
    Scene* old_destination_scene = m_destination_scene;


    m_source_scene = new Scene(this);
    m_source_scene->setWindow(this);
    m_source_scene->disableArrowMoving();

    m_destination_scene = new Scene(this);
    m_destination_scene->setWindow(this);

    ui.grpSourceTree->setScene(m_source_scene);
    m_source_scene->setSceneRect(0, 0, ui.grpSourceTree->width() - 2, ui.grpSourceTree->height() - 2);
    ui.grpTransformedTree->setScene(m_destination_scene);
    m_destination_scene->setSceneRect(0, 0, ui.grpTransformedTree->width() - 2, ui.grpTransformedTree->height() - 2);

    ui.grpSourceTree->setMouseTracking(true);
    ui.grpSourceTree->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);

    ui.grpTransformedTree->setMouseTracking(true);
    ui.grpTransformedTree->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);

    delete old_source_scene;
    delete old_destination_scene;

    for(int i = m_history_items.size() - 1; i > -1; i--)
    {
        delete m_history_items[i];
    }

    m_history_items.clear();
    m_history_cursor_position = 0;
    ui.txtExpression->setPlainText("");
}

void MainWindow::saveAs()
{
    QString result = QFileDialog::getSaveFileName(
        this,
        "Please, point, where you want file to be saved",
        "",
        "XML (*.xml)"
   );
    if (result.length() != 0)
    {
        m_file_name = result;
        save();
    }
}


void MainWindow::save()
{
    if (m_file_name.length() == 0)
    {
        this->saveAs();
        return;
    }

    QDomDocument doc("Graph");
    QDomElement root = doc.createElement("Graph");
    root.setAttribute("Text", ui.txtExpression->toPlainText());

    QDomElement source = this->m_source_scene->toElement(doc);
    source.setAttribute("Type", "Source");
    root.appendChild(source);

    QDomElement dest = this->m_destination_scene->toElement(doc);
    dest.setAttribute("Type", "Destination");
    root.appendChild(dest);

    doc.appendChild(root);

    QString doc_as_string = doc.toString();
    QFile file(m_file_name);
    if (file.open(QIODevice::WriteOnly))
    {
        QTextStream stream(&file);
        stream << doc_as_string;
        file.close();

        this->setWindowTitle(QString("Graph Rewriting Rules Extractor - ")  + m_file_name);
    }
    else
    {
        QMessageBox::critical(NULL, "Error", "Cannot open file for writing");
    }
}

void MainWindow::load()
{
    QString file_name = QFileDialog::getOpenFileName(
        this,
        "Select file with stored graph data",
        "",
        "XML (*.xml)"
    );
    if (file_name.length() == 0)
    {
        return;
    }
    bool ok = true;
    QString content = MainWindow::tryReadFile(file_name, &ok);
    if (ok)
    {
        QDomDocument document;
        if (document.setContent(content))
        {
            QDomElement root = document.documentElement();
            QString text = root.attribute("Text");
            Scene* source = NULL,* dest = NULL;
            bool error = false;
            QDomNode n = root.firstChild();
            while (!n.isNull() && !error)
            {
                QDomElement ce = n.toElement();  // Convert to element
                if (!ce.isNull())
                {
                   if (ce.tagName() =="Scene")
                   {
                       if (ce.attribute("Type") == "Source")
                       {
                           if (source != NULL)
                           {
                               QMessageBox::critical(NULL, "Critical error", "Duplicate source scene in loaded file");
                               error = true;
                           }
                           else
                           {
                                source = new Scene();
                                source->setWindow(this);
                                source->disableArrowMoving();
                                error = !(source->loadFromElement(ce));
                                if (error)
                                {
                                    QMessageBox::critical(NULL, "Critical error", "Failed to load a scene");
                                }
                           }
                       }
                       else
                       {
                           if (ce.attribute("Type") == "Destination")
                           {
                               if (dest != NULL)
                               {
                                   QMessageBox::critical(NULL, "Critical error", "Duplicate destination scene in loaded file");
                                   error = true;
                               }
                               else
                               {
                                   dest = new Scene();
                                   dest->setWindow(this);
                                   error = !(dest->loadFromElement(ce));
                                   if (error)
                                   {
                                       QMessageBox::critical(NULL, "Critical error", "Failed to load a scene");
                                   }
                               }
                           }
                           else
                           {
                               QMessageBox::critical(NULL, "Critical error", "Unknown type of scene in loaded file");
                               error = true;
                           }
                       }
                   }
                }
                n = n.nextSibling();
            }
            if (error)
            {
                delete source;
                delete dest;
            }
            else
            {
                this->newFile();
                ui.txtExpression->setPlainText(text);

                Scene* old_source_scene = m_source_scene;
                Scene* old_destination_scene = m_destination_scene;
                ui.grpSourceTree->setScene(source);
                ui.grpTransformedTree->setScene(dest);

                delete old_source_scene;
                delete old_destination_scene;

                m_source_scene = source;
                m_destination_scene = dest;

                m_source_scene->expand();
                m_destination_scene->expand();

                m_file_name = file_name;
                this->setWindowTitle(QString("Graph Rewriting Rules Extractor - ")  + m_file_name);
            }
        }
        else
        {
            QMessageBox::critical(NULL, "Critical failure", "Unable to parse output XML data");
        }
    }
    else
    {
         QMessageBox::critical(NULL, "Error", "Cannot load file for opening");
    }
 }

void MainWindow::newWindow() const
{
    MainWindow* win = new MainWindow(m_app);
    win->show();

    AdditionalWindows << win;
}

//  ================================= PRIVATE METHODS =================================


bool MainWindow::writeParsedString(const QString& string) const
{
    QFile file(m_parser_path + "in.txt");
    if (file.open(QIODevice::WriteOnly))
    {
        {
            QTextStream stream(&file);
            stream << string;
        }
        file.close();
    }
    else
    {
      return false;
    }
    return true;
}

QString MainWindow::tryReadParsedFile(bool* ok) const
{
    return MainWindow::tryReadFile(m_parser_path + "out.xml", ok);
}

QString MainWindow::tryReadFile(const QString& file_name, bool* ok)
{
    *ok = false;
    QFile file(file_name);
    QString string;
    if (file.open(QIODevice::ReadOnly))
    {
        {
            QTextStream stream(&file);
            string.append(stream.readAll());
            *ok = true;
        }
        file.close();
    }
    return string;
}

MainWindow::XMLNode* MainWindow::parse(
    const QDomElement& e,
    QHash<MainWindow::XMLNode*, ::Node*>& storage,
    Scene* s
) const
{
    MainWindow::XMLNode* result = new MainWindow::XMLNode();
    Node* node = new Node();
    QString text = e.tagName();
    if (!e.hasChildNodes())
    {
        text.append("\n").append(e.attribute("value"));
    }
    node->setText(text);
    node->Id = s->getId();
    s->addNode(node);
    s->addItem(node);
    storage.insert(result, node);
    QDomNode n = e.firstChild();
    while(!n.isNull())
    {
        QDomElement ce = n.toElement();  // Convert to element
        if (!ce.isNull())
        {
           result->Nodes << this->parse(ce, storage, s);
        }
        n = n.nextSibling();
    }
    return result;
}


void MainWindow::layoutNodes(double leftx, double topy, MainWindow::XMLNode* node, const QHash<MainWindow::XMLNode*, ::Node*>& storage)
{
    ::Node* globalNode = storage[node];
    Scene* scene = dynamic_cast<Scene*>(globalNode->scene());
    double localwidth = globalNode->sceneBoundingRect().width();
    double totalwidth = 0;
    for (int i = 0; i < node->Nodes.size(); i++)
    {
        if (i != 0)
        {
            totalwidth += LAYOUT_PADDING_X;
        }
        totalwidth += getWidthRecursive(node->Nodes[i], storage);
    }
    double wholewidth = std::max(localwidth, totalwidth);
    double localtopy = topy + LAYOUT_PADDING_Y + globalNode->sceneBoundingRect().height();
    globalNode->setPos(leftx  + wholewidth / 2.0 - localwidth / 2.0, localtopy);
    double beginx = leftx + wholewidth / 2.0 - totalwidth / 2.0;
    for (int i = 0; i < node->Nodes.size(); i++)
    {
        if (i != 0)
        {
            beginx += LAYOUT_PADDING_X;
        }
        layoutNodes(beginx, localtopy, node->Nodes[i], storage);
        QPointF localcenter = globalNode->sceneBoundingRect().center();
        QPointF tocenter = storage[node->Nodes[i]]->sceneBoundingRect().center();
        QLineF line(localcenter, tocenter);
        QPointF start = collisionPoint(line, globalNode->sceneBoundingRect()).value;
        QPointF end = collisionPoint(line, storage[node->Nodes[i]]->sceneBoundingRect()).value;
        line.setLine(start.x(), start.y(),  end.x(), end.y());
        Arrow* arrow = new Arrow();
        arrow->setLine(line);
        arrow->Id = scene->getId();
        arrow->FromId = {globalNode->Id};
        arrow->ToId =  {storage[node->Nodes[i]]->Id};
        scene->addArrow(arrow);
        scene->addItem(arrow);

        beginx += getWidthRecursive(node->Nodes[i], storage);
    }
}

void MainWindow::recomputeSceneBounds(Scene* scene, const QHash<MainWindow::XMLNode*, ::Node*>& storage)
{
    double rightx = 0;
    double bottomy = 0;
    for(QHash<MainWindow::XMLNode*, ::Node*>::const_iterator it = storage.begin();
        it != storage.end();
        ++it)
    {
        rightx = std::max(rightx, it.value()->sceneBoundingRect().right() + LAYOUT_PADDING_X);
        bottomy = std::max(bottomy, it.value()->sceneBoundingRect().bottom()  + LAYOUT_PADDING_Y);
    }
    scene->setSceneRect(0, 0, rightx, bottomy);
}

double MainWindow::getWidthRecursive(MainWindow::XMLNode* node, const QHash<MainWindow::XMLNode*, ::Node*>& storage)
{
    ::Node* globalNode = storage[node];
    double localwidth = globalNode->sceneBoundingRect().width();
    double totalwidth = 0;
    for(int i = 0; i < node->Nodes.size(); i++)
    {
        if (i != 0)
        {
            totalwidth += LAYOUT_PADDING_X;
        }
        totalwidth += getWidthRecursive(node->Nodes[i], storage);
    }
    return std::max(localwidth, totalwidth);
}

// ================================ NewParseItem ================================

MainWindow::NewParseItem::NewParseItem() 
: OldSourceScene(NULL), OldDestinationScene(NULL), NewSourceScene(NULL), NewDestinationScene(NULL), OwnedScene1(NULL), OwnedScene2(NULL), Window(NULL)
{
    
}

MainWindow::NewParseItem::~NewParseItem()
{
    delete OwnedScene1;
    delete OwnedScene2;
}

void MainWindow::NewParseItem::undo()
{
    Window->setScenes(OldSourceScene, OldDestinationScene);

    OwnedScene1 = NewSourceScene;
    OwnedScene2 = NewDestinationScene;

}

void MainWindow::NewParseItem::redo()
{
    Window->setScenes(NewSourceScene, NewDestinationScene);

    OwnedScene1 = OldSourceScene;
    OwnedScene2 = OldDestinationScene;
}


// ================================ CopyItem ================================

MainWindow::CopyItem::CopyItem()
: OldDestinationScene(NULL), NewDestinationScene(NULL), OwnedScene(NULL), Window(NULL)
{

}

MainWindow::CopyItem::~CopyItem()
{
    delete OwnedScene;
}

void MainWindow::CopyItem::undo()
{
    Window->setDestinationScene(OldDestinationScene);

    OwnedScene = NewDestinationScene;
}

void MainWindow::CopyItem::redo()
{
    Window->setDestinationScene(NewDestinationScene);

    OwnedScene = OldDestinationScene;
}

// ================================ MoveNodeItem ================================

MainWindow::MoveChangeTextNode::MoveChangeTextNode() : Node(NULL)
{

}

MainWindow::MoveChangeTextNode::MoveChangeTextNode(const Scene::NodeChange& change)
: Node(change.Node),
OldText(change.OldText),
NewText(change.NewText),
OldPosition(change.OldPosition),
NewPosition(change.NewPosition),
ArrowsComingFromNode(change.ArrowsComingFromNode),
ArrowsComingFromNodeOldPositions(change.ArrowsComingFromNodeOldPositions),
ArrowsComingFromNodeNewPositions(change.ArrowsComingFromNodeNewPositions),
ArrowsComingToNode(change.ArrowsComingToNode),
ArrowsComingToNodeOldPositions(change.ArrowsComingToNodeOldPositions),
ArrowsComingToNodeNewPositions(change.ArrowsComingToNodeNewPositions),
LinesComingFromNode(change.LinesComingFromNode),
LinesComingFromNodeOldPositions(change.LinesComingFromNodeOldPositions),
LinesComingFromNodeNewPositions(change.LinesComingFromNodeNewPositions)  // NOLINT
{

}

// NOLINT
MainWindow::MoveChangeTextNode::~MoveChangeTextNode() // NOLINT
{
    
}


void MainWindow::MoveChangeTextNode::MoveChangeTextNode::undo()
{
    Node->setPos(this->OldPosition.topLeft());
    Node->setText(this->OldText);
    for(int i = 0; i < ArrowsComingFromNode.size(); i++)
    {
        QLineF line =  ArrowsComingFromNode[i]->line();
        line.setP1(ArrowsComingFromNodeOldPositions[i]);
        ArrowsComingFromNode[i]->setLine(line);
    }
    for(int i = 0; i < ArrowsComingToNode.size(); i++)
    {
        QLineF line =  ArrowsComingToNode[i]->line();
        line.setP2(ArrowsComingToNodeOldPositions[i]);
        ArrowsComingToNode[i]->setLine(line);
    }
    for(int i = 0; i < LinesComingFromNode.size(); i++)
    {
        QLineF line =  LinesComingFromNode[i]->line();
        line.setP1(LinesComingFromNodeOldPositions[i]);
        LinesComingFromNode[i]->setLine(line);
    }
}

void MainWindow::MoveChangeTextNode::redo()
{
    Node->setPos(this->NewPosition.topLeft());
    Node->setText(this->NewText);

    for(int i = 0; i < ArrowsComingFromNode.size(); i++)
    {
        QLineF line =  ArrowsComingFromNode[i]->line();
        line.setP1(ArrowsComingFromNodeNewPositions[i]);
        ArrowsComingFromNode[i]->setLine(line);
    }
    for(int i = 0; i < ArrowsComingToNode.size(); i++)
    {
        QLineF line =  ArrowsComingToNode[i]->line();
        line.setP2(ArrowsComingToNodeNewPositions[i]);
        ArrowsComingToNode[i]->setLine(line);
    }
    for(int i = 0; i < LinesComingFromNode.size(); i++)
    {
        QLineF line =  LinesComingFromNode[i]->line();
        line.setP1(LinesComingFromNodeNewPositions[i]);
        LinesComingFromNode[i]->setLine(line);
    }
}

// ================================ ArrowChange ================================

MainWindow::ArrowChange::ArrowChange() : LocalArrow(NULL), First(false)
{

}

// NOLINT
MainWindow::ArrowChange::~ArrowChange() // NOLINT
{
    
}


void MainWindow::ArrowChange::undo()
{
    QLineF line = LocalArrow->line();
    if (First)
    {
        line.setP1(OldPoint);
        LocalArrow->FromId = OldValue;
    }
    else
    {
        line.setP2(OldPoint);
        LocalArrow->ToId = OldValue;
    }
    LocalArrow->setLine(line);
}

void MainWindow::ArrowChange::redo()
{
    QLineF line = LocalArrow->line();
    if (First)
    {
        line.setP1(NewPoint);
        LocalArrow->FromId = NewValue;
    }
    else
    {
        line.setP2(NewPoint);
        LocalArrow->ToId = NewValue;
    }
    LocalArrow->setLine(line);
}

// ================================ AddNode ================================

MainWindow::AddNode::AddNode() : LocalNode(NULL), Scene(NULL)
{

}

// NOLINT
MainWindow::AddNode::~AddNode() // NOLINT
{
    
}

void MainWindow::AddNode::undo()
{
    Scene->hideAndRemoveNode(LocalNode);
}

void MainWindow::AddNode::redo()
{
    Scene->showAndAddNode(LocalNode);
}

// =============================== RemoveNode ================================

MainWindow::RemoveNode::RemoveNode() : Node(NULL), Scene(NULL)
{

}

// NOLINT
MainWindow::RemoveNode::~RemoveNode() // NOLINT
{
    
}

void MainWindow::RemoveNode::undo()
{
    Scene->showAndAddNode(Node);
    for (auto& AttachedFromArrow : AttachedFromArrows)
    {
        AttachedFromArrow->FromId = {Node->Id};
    }

    for (auto& AttachedToArrow : AttachedToArrows)
    {
        AttachedToArrow->ToId = {Node->Id};
    }

    for(auto& AttachedLine : AttachedLines)
    {
        AttachedLine->FromId = {Node->Id};
    }
}


void MainWindow::RemoveNode::redo()
{
    Scene->hideAndRemoveNode(Node);
    for (auto& AttachedFromArrow : AttachedFromArrows)
    {
        AttachedFromArrow->FromId =  Maybe<unsigned int>();
    }

    for (auto& AttachedToArrow : AttachedToArrows)
    {
        AttachedToArrow->ToId = Maybe<unsigned int>();
    }

    for(auto& AttachedLine : AttachedLines)
    {
        AttachedLine->FromId = Maybe<unsigned int>();
    }
}


// =============================== AddArrow ================================


MainWindow::AddArrow::AddArrow() : LocalArrow(NULL), Scene(NULL)
{

}

// NOLINT
MainWindow::AddArrow::~AddArrow() // NOLINT
{

}

void MainWindow::AddArrow::undo()
{
    Scene->hideAndRemoveArrow(LocalArrow);
}

void MainWindow::AddArrow::redo()
{
    Scene->showAndAddArrow(LocalArrow);
}


// =============================== RemoveArrow ================================

MainWindow::RemoveArrow::RemoveArrow() : Arrow(NULL), Scene(NULL)
{

}

// NOLINT
MainWindow::RemoveArrow::~RemoveArrow() // NOLINT
{

}

void MainWindow::RemoveArrow::undo()
{
    Scene->showAndAddArrow(Arrow);
}


void MainWindow::RemoveArrow::redo()
{
    Scene->hideAndRemoveArrow(Arrow);
}

// ================================ AddAnnotationRule ================================

MainWindow::AddAnnotationRule::AddAnnotationRule() : LocalNode(NULL), Scene(NULL)
{

}

// NOLINT
MainWindow::AddAnnotationRule::~AddAnnotationRule() // NOLINT
{

}

void MainWindow::AddAnnotationRule::undo()
{
    Scene->hideAndRemoveAnnotationRule(LocalNode);
}

void MainWindow::AddAnnotationRule::redo()
{
    Scene->showAndAddAnnotationRule(LocalNode);
}


// ================================================= ChangeAnnotationRuleItem =================================================


// NOLINT
MainWindow::ChangeAnnotationRuleItem::ChangeAnnotationRuleItem() // NOLINT
{

}

MainWindow::ChangeAnnotationRuleItem::ChangeAnnotationRuleItem(const Scene::AnnotationRuleChange& change) : Change(change)
{

}

MainWindow::ChangeAnnotationRuleItem::~ChangeAnnotationRuleItem()
{

}


void MainWindow::ChangeAnnotationRuleItem::undo()
{
    Change.Rule->setMaximalRect(this->Change.OldMaxRect);
    Change.Rule->setPos(this->Change.OldRect.topLeft());
    Change.Rule->setText(this->Change.OldText);

    for(int i = 0; i < Change.Lines.size(); i++)
    {
        QLineF line =  Change.Lines[i]->line();
        line.setP2(Change.OldPositions[i]);
        Change.Lines[i]->setLine(line);
    }
}

void MainWindow::ChangeAnnotationRuleItem::redo()
{
    Change.Rule->setMaximalRect(this->Change.NewMaxRect);
    Change.Rule->setPos(this->Change.NewRect.topLeft());
    Change.Rule->setText(this->Change.NewText);

    for(int i = 0; i < Change.Lines.size(); i++)
    {
        QLineF line =  Change.Lines[i]->line();
        line.setP2(Change.NewPositions[i]);
        Change.Lines[i]->setLine(line);
    }
}

// ====================================================== RemoveAnnotationRule ======================================================

MainWindow::RemoveAnnotationRule::RemoveAnnotationRule() : Rule(NULL), Scene(NULL)
{

}

// NOLINT
MainWindow::RemoveAnnotationRule::~RemoveAnnotationRule() // NOLINT
{

}

void MainWindow::RemoveAnnotationRule::undo()
{
    Scene->showAndAddAnnotationRule(Rule);

    for (auto& AttachedLine : AttachedLines)
    {
        AttachedLine->ToId = {Rule->Id};
    }
}


void MainWindow::RemoveAnnotationRule::redo()
{
    Scene->hideAndRemoveAnnotationRule(Rule);

    for (auto& AttachedLine : AttachedLines)
    {
        AttachedLine->ToId = Maybe<unsigned int>();
    }
}

// ====================================================== AddAnnotationLine ======================================================


MainWindow::AddAnnotationLine::AddAnnotationLine() : LocalAnnotationLine(NULL), Scene(NULL)
{

}

// NOLINT
MainWindow::AddAnnotationLine::~AddAnnotationLine() // NOLINT
{

}

void MainWindow::AddAnnotationLine::undo()
{
    Scene->hideAndRemoveAnnotationLine(LocalAnnotationLine);
}

void MainWindow::AddAnnotationLine::redo()
{
    Scene->showAndAddAnnotationLine(LocalAnnotationLine);
}



// =============================== RemoveAnnotationLine ================================

MainWindow::RemoveAnnotationLine::RemoveAnnotationLine() : LocalAnnotationLine(NULL), Scene(NULL)
{

}

// NOLINT
MainWindow::RemoveAnnotationLine::~RemoveAnnotationLine() // NOLINT
{

}

void MainWindow::RemoveAnnotationLine::undo()
{
    Scene->showAndAddAnnotationLine(LocalAnnotationLine);
}


void MainWindow::RemoveAnnotationLine::redo()
{
    Scene->hideAndRemoveAnnotationLine(LocalAnnotationLine);
}

// ==================================== AnnotationLineChange =================================

MainWindow::AnnotationLineChange::AnnotationLineChange() : LocalAnnotationLine(NULL)
{

}

// NOLINT
MainWindow::AnnotationLineChange::~AnnotationLineChange() // NOLINT
{

}

void MainWindow::AnnotationLineChange::undo()
{
    LocalAnnotationLine->setLine(OldLine);
    LocalAnnotationLine->FromId = OldFromValue;
    LocalAnnotationLine->ToId = OldToValue;
}


void MainWindow::AnnotationLineChange::redo()
{
    LocalAnnotationLine->setLine(NewLine);
    LocalAnnotationLine->FromId = NewFromValue;
    LocalAnnotationLine->ToId = NewToValue;
}

