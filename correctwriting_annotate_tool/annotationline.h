/*! \file annotationline.h
*
*   An annotationline, that will be used to render some arrows in editor
*/
#pragma once
#include "utils.h"
#include <QGraphicsLineItem>
#include <QDomElement>

class AnnotationLine : public QGraphicsLineItem
{
public:
    /*! An arrow
    */
    AnnotationLine();
    /*! Clones an arrow
     *  \return a cloned arrow
     */
    AnnotationLine* clone() const;
    /*! Returns an element, that stores an annotation line
     *  \param[in] doc document
     *  \return element
     */
    QDomElement toElement(QDomDocument& doc);
    /*! Loads item from element
     *  \param[in] element
     *  \return result
     */
    bool loadFromElement(QDomElement& e);
    /*! A from node id
     */
    Maybe<unsigned int> FromId;
    /*! A to node id
     */
    Maybe<unsigned int> ToId;
    /*! A local id
     */
    unsigned int Id;
};

