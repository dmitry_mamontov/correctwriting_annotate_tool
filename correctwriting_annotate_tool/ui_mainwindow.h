/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionNew;
    QAction *action_Open;
    QAction *action_Save;
    QAction *actionS_ave_as;
    QAction *action_Exit;
    QAction *actionSpecify_path;
    QAction *actionNewWindow;
    QWidget *centralwidget;
    QLabel *lblLabel;
    QGraphicsView *grpSourceTree;
    QLabel *lblSourceTree;
    QPushButton *btnParse;
    QGraphicsView *grpTransformedTree;
    QLabel *lblTransformedTree;
    QPushButton *btnAddEdge;
    QPushButton *btnRemove;
    QPushButton *btnAddNode;
    QPlainTextEdit *txtExpression;
    QLabel *lblStatus;
    QPushButton *btnAddAnnotation;
    QPushButton *btnConnectAWN;
    QPushButton *btnUndo;
    QPushButton *btnRedo;
    QPlainTextEdit *txtEdit;
    QMenuBar *menubar;
    QMenu *menuFile;
    QMenu *menuRule_extraction;
    QMenu *menuParser;
    QMenu *menuWindow;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1272, 681);
        actionNew = new QAction(MainWindow);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        action_Open = new QAction(MainWindow);
        action_Open->setObjectName(QStringLiteral("action_Open"));
        action_Save = new QAction(MainWindow);
        action_Save->setObjectName(QStringLiteral("action_Save"));
        actionS_ave_as = new QAction(MainWindow);
        actionS_ave_as->setObjectName(QStringLiteral("actionS_ave_as"));
        action_Exit = new QAction(MainWindow);
        action_Exit->setObjectName(QStringLiteral("action_Exit"));
        actionSpecify_path = new QAction(MainWindow);
        actionSpecify_path->setObjectName(QStringLiteral("actionSpecify_path"));
        actionNewWindow = new QAction(MainWindow);
        actionNewWindow->setObjectName(QStringLiteral("actionNewWindow"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        lblLabel = new QLabel(centralwidget);
        lblLabel->setObjectName(QStringLiteral("lblLabel"));
        lblLabel->setGeometry(QRect(10, 11, 81, 16));
        grpSourceTree = new QGraphicsView(centralwidget);
        grpSourceTree->setObjectName(QStringLiteral("grpSourceTree"));
        grpSourceTree->setGeometry(QRect(10, 60, 620, 501));
        lblSourceTree = new QLabel(centralwidget);
        lblSourceTree->setObjectName(QStringLiteral("lblSourceTree"));
        lblSourceTree->setGeometry(QRect(310, 40, 71, 16));
        btnParse = new QPushButton(centralwidget);
        btnParse->setObjectName(QStringLiteral("btnParse"));
        btnParse->setGeometry(QRect(870, 10, 75, 31));
        grpTransformedTree = new QGraphicsView(centralwidget);
        grpTransformedTree->setObjectName(QStringLiteral("grpTransformedTree"));
        grpTransformedTree->setGeometry(QRect(640, 60, 620, 501));
        lblTransformedTree = new QLabel(centralwidget);
        lblTransformedTree->setObjectName(QStringLiteral("lblTransformedTree"));
        lblTransformedTree->setGeometry(QRect(990, 40, 101, 16));
        btnAddEdge = new QPushButton(centralwidget);
        btnAddEdge->setObjectName(QStringLiteral("btnAddEdge"));
        btnAddEdge->setGeometry(QRect(810, 570, 161, 23));
        btnRemove = new QPushButton(centralwidget);
        btnRemove->setObjectName(QStringLiteral("btnRemove"));
        btnRemove->setGeometry(QRect(1150, 10, 111, 31));
        btnAddNode = new QPushButton(centralwidget);
        btnAddNode->setObjectName(QStringLiteral("btnAddNode"));
        btnAddNode->setGeometry(QRect(640, 570, 161, 23));
        txtExpression = new QPlainTextEdit(centralwidget);
        txtExpression->setObjectName(QStringLiteral("txtExpression"));
        txtExpression->setGeometry(QRect(70, 10, 791, 31));
        lblStatus = new QLabel(centralwidget);
        lblStatus->setObjectName(QStringLiteral("lblStatus"));
        lblStatus->setGeometry(QRect(640, 630, 331, 21));
        btnAddAnnotation = new QPushButton(centralwidget);
        btnAddAnnotation->setObjectName(QStringLiteral("btnAddAnnotation"));
        btnAddAnnotation->setGeometry(QRect(640, 600, 161, 23));
        btnConnectAWN = new QPushButton(centralwidget);
        btnConnectAWN->setObjectName(QStringLiteral("btnConnectAWN"));
        btnConnectAWN->setGeometry(QRect(810, 600, 161, 23));
        btnUndo = new QPushButton(centralwidget);
        btnUndo->setObjectName(QStringLiteral("btnUndo"));
        btnUndo->setGeometry(QRect(960, 10, 75, 31));
        btnRedo = new QPushButton(centralwidget);
        btnRedo->setObjectName(QStringLiteral("btnRedo"));
        btnRedo->setGeometry(QRect(1040, 10, 75, 31));
        txtEdit = new QPlainTextEdit(centralwidget);
        txtEdit->setObjectName(QStringLiteral("txtEdit"));
        txtEdit->setGeometry(QRect(980, 570, 281, 81));
        txtEdit->setMaximumBlockCount(0);
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 1272, 21));
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuRule_extraction = new QMenu(menubar);
        menuRule_extraction->setObjectName(QStringLiteral("menuRule_extraction"));
        menuParser = new QMenu(menubar);
        menuParser->setObjectName(QStringLiteral("menuParser"));
        menuWindow = new QMenu(menubar);
        menuWindow->setObjectName(QStringLiteral("menuWindow"));
        MainWindow->setMenuBar(menubar);

        menubar->addAction(menuFile->menuAction());
        menubar->addAction(menuRule_extraction->menuAction());
        menubar->addAction(menuParser->menuAction());
        menubar->addAction(menuWindow->menuAction());
        menuFile->addAction(actionNew);
        menuFile->addAction(action_Open);
        menuFile->addAction(action_Save);
        menuFile->addAction(actionS_ave_as);
        menuFile->addAction(action_Exit);
        menuParser->addAction(actionSpecify_path);
        menuWindow->addAction(actionNewWindow);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Graph Rewriting Rules Extractor", nullptr));
        actionNew->setText(QApplication::translate("MainWindow", "&New", nullptr));
        action_Open->setText(QApplication::translate("MainWindow", "&Open", nullptr));
        action_Save->setText(QApplication::translate("MainWindow", "&Save", nullptr));
        actionS_ave_as->setText(QApplication::translate("MainWindow", "S&ave as...", nullptr));
        action_Exit->setText(QApplication::translate("MainWindow", "&Exit", nullptr));
        actionSpecify_path->setText(QApplication::translate("MainWindow", "Specify path", nullptr));
        actionNewWindow->setText(QApplication::translate("MainWindow", "New Window", nullptr));
        lblLabel->setText(QApplication::translate("MainWindow", "Expression:", nullptr));
        lblSourceTree->setText(QApplication::translate("MainWindow", "Source tree:", nullptr));
        btnParse->setText(QApplication::translate("MainWindow", "Parse", nullptr));
        lblTransformedTree->setText(QApplication::translate("MainWindow", "Transformed tree:", nullptr));
        btnAddEdge->setText(QApplication::translate("MainWindow", "Add Edge", nullptr));
        btnRemove->setText(QApplication::translate("MainWindow", "Remove object", nullptr));
        btnAddNode->setText(QApplication::translate("MainWindow", "Add Node", nullptr));
        lblStatus->setText(QString());
        btnAddAnnotation->setText(QApplication::translate("MainWindow", "Annotate...", nullptr));
        btnConnectAWN->setText(QApplication::translate("MainWindow", "Connect annotation with node", nullptr));
        btnUndo->setText(QApplication::translate("MainWindow", "Undo", nullptr));
        btnRedo->setText(QApplication::translate("MainWindow", "Redo", nullptr));
        txtEdit->setPlaceholderText(QApplication::translate("MainWindow", "Node", nullptr));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", nullptr));
        menuRule_extraction->setTitle(QApplication::translate("MainWindow", "Rule extraction", nullptr));
        menuParser->setTitle(QApplication::translate("MainWindow", "Parser", nullptr));
        menuWindow->setTitle(QApplication::translate("MainWindow", "Window", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
