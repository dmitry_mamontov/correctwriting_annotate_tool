#include "annotationline.h"

#include <QBrush>
#include <QPen>

AnnotationLine::AnnotationLine()
{
    QBrush brush(QColor(0,0,0));
    QPen pen(brush, 1, Qt::DashLine);
    setPen(pen);
}

AnnotationLine* AnnotationLine::clone() const
{
    AnnotationLine* a = new AnnotationLine();
    a->setLine(this->line());
    a->Id = this->Id;
    a->FromId = this->FromId;
    a->ToId = this->ToId;
    return a;
}

QDomElement AnnotationLine::toElement(QDomDocument& doc)
{
    QDomElement e = doc.createElement("AnnotationLine");
    e.setAttribute("Id", toString(this->Id));

    e.setAttribute("P1", toString(this->line().p1()));
    e.setAttribute("P2", toString(this->line().p2()));
    e.setAttribute("FromId", toString(this->FromId));
    e.setAttribute("ToId", toString(this->ToId));

    return e;
}


bool AnnotationLine::loadFromElement(QDomElement& e)
{
    Maybe<unsigned int> id;
    Maybe<Maybe<unsigned int> > from_id;
    Maybe<Maybe<unsigned int> > to_id;

    Maybe<QPointF> p1, p2;

    parse(e.attribute("Id"), id);
    parse(e.attribute("P1"), p1);
    parse(e.attribute("P2"), p2);
    parse(e.attribute("FromId"), from_id);
    parse(e.attribute("ToId"), to_id);

    if (id.exists && p1.exists && p2.exists && from_id.exists && to_id.exists)
    {
        this->Id = id.value;
        this->FromId = from_id.value;
        this->ToId = to_id.value;
        this->setLine(QLineF(p1.value, p2.value));
        return true;
    }

    return false;
}
