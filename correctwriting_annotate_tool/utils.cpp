#include "utils.h"

bool isWithin(const QPointF& p, const QRectF& r)
{
    return r.contains(p);
}

Maybe<QPointF> collisionPoint(const QLineF& l, const QRectF& r)
{
    QPointF result;
    QLineF sides[4]  = {
        QLineF(r.topLeft(), r.topRight()),
        QLineF(r.topLeft(), r.bottomLeft()),
        QLineF(r.topRight(), r.bottomRight()),
        QLineF(r.bottomLeft(), r.bottomRight())
    };
    for (const auto& side : sides)
    {
        if (l.intersect(side, &result) == QLineF::BoundedIntersection)
        {
            return {result};
        }
    }
    return {};
}