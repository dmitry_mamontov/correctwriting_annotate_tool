/*! \file node.h
 *   A node a data of tree data
 */
#pragma once
#include <QGraphicsItem>
#include <QDomElement>

/*! A node data
 */
class Node: public QGraphicsItem
{
public:
    Node();
    /*! An id for rule
    */
    unsigned int Id;
    /*! Clones a node
     */
    Node* clone() const;
    /*! Returns a bounding rectangle for rule
     *  \return rectangle
     */
    virtual QRectF boundingRect() const;
    /*! Sets text
     *  \param[in] text a text
     */
    void setText(const QString& text);
    /*! Returns a text
     *  \return a text
     */
    const QString& text() const;
    /*! Returns an element, that stores a node
     *  \param[in] doc document
     *  \return element
     */
    QDomElement toElement(QDomDocument& doc);
    /*! Loads item from element
     *  \param[in] element
     *  \return result
     */
    bool loadFromElement(QDomElement& e);
protected:
    /*! Paints an annotation
    */
    virtual void paint(QPainter* p, const QStyleOptionGraphicsItem* option, QWidget* widget);
    /*! A text
     */
    QString m_text;
    /*! A rectangle
     */
    QRectF m_rect;
};
