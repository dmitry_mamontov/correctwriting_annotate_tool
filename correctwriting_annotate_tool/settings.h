/*! A main settings macroses for program
 */
#pragma once

// A font
#define FONT "Georgia"
// A font size
#define FONT_SIZE 14
// A max size parameter
#define MAX_SIZE_PARAMETER 10000
// An annotation font size
#define ANNOTATION_FONT_SIZE 11
