#pragma once

// ReSharper disable once CppUnusedIncludeDirective
#include <QtWidgets/QMainWindow>
#include "ui_mainwindow.h"

#include <functional>

#include <QString>
#include <QVector>
#include <QDomDocument>
#include "scene.h"


class Node;

/*! A maindata
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT
    /*! An XML node for parsing
     */
    struct XMLNode
    {
        QString Data; //!< A data
        QVector<MainWindow::XMLNode*> Nodes; //!< A list of nodes

        inline ~XMLNode()
        {
            for (int i = 0; i < Nodes.size(); i++)
            {
                delete Nodes[i];
            }
        }
    };
public:
    struct HistoryItem
    {
    public:
        virtual void undo() = 0;
        virtual void redo() = 0;
        virtual ~HistoryItem() = default;
    };

    struct NewParseItem: public HistoryItem
    {
    public:
        Scene* OldSourceScene;
        Scene* OldDestinationScene;
        Scene* NewSourceScene;
        Scene* NewDestinationScene;

        Scene* OwnedScene1;
        Scene* OwnedScene2;

        MainWindow* Window;

        NewParseItem();
        ~NewParseItem();

        virtual void undo();
        virtual void redo();
    };

    struct CopyItem: public HistoryItem
    {
    public:
        Scene* OldDestinationScene;
        Scene* NewDestinationScene;

        Scene* OwnedScene;

        MainWindow* Window;

        CopyItem();
        ~CopyItem();

        virtual void undo();
        virtual void redo();
    };

    struct MoveChangeTextNode: public HistoryItem
    {
        Node* Node;            // A current starting node

        QString OldText;       // An old text
        QString NewText;       // A new text

        QRectF OldPosition;    // An old position
        QRectF NewPosition;    // A new position

        QVector<Arrow*> ArrowsComingFromNode;
        QVector<QPointF> ArrowsComingFromNodeOldPositions;
        QVector<QPointF> ArrowsComingFromNodeNewPositions;

        QVector<Arrow*> ArrowsComingToNode;
        QVector<QPointF> ArrowsComingToNodeOldPositions;
        QVector<QPointF> ArrowsComingToNodeNewPositions;


        QVector<AnnotationLine*> LinesComingFromNode;
        QVector<QPointF> LinesComingFromNodeOldPositions;
        QVector<QPointF> LinesComingFromNodeNewPositions;


        MoveChangeTextNode();
        MoveChangeTextNode(const Scene::NodeChange& change);
        ~MoveChangeTextNode();

        virtual void undo();
        virtual void redo();
    };

    struct ArrowChange: public HistoryItem
    {
        Arrow* LocalArrow;

        Maybe<unsigned int> OldValue;
        Maybe<unsigned int> NewValue;

        QPointF OldPoint;
        QPointF NewPoint;

        bool First;

        ArrowChange();
        ~ArrowChange();

        virtual void undo();
        virtual void redo();
    };

    struct AddNode: public HistoryItem
    {
        Node* LocalNode;
        Scene* Scene;

        AddNode();
        ~AddNode();

        virtual void undo();
        virtual void redo();
    };

    struct RemoveNode: public HistoryItem
    {
        Node* Node; // A current node
        Scene* Scene;

        QVector<Arrow*> AttachedFromArrows;
        QVector<Arrow*> AttachedToArrows;
        QVector<AnnotationLine*> AttachedLines;

        RemoveNode();
        ~RemoveNode();

        virtual void undo();
        virtual void redo();
    };

    struct AddArrow: public HistoryItem
    {
        Arrow* LocalArrow;
        Scene* Scene;

        AddArrow();
        ~AddArrow();

        virtual void undo();
        virtual void redo();
    };

    struct RemoveArrow: public HistoryItem
    {
        Arrow* Arrow; // A current node
        Scene* Scene;

        RemoveArrow();
        ~RemoveArrow();

        virtual void undo();
        virtual void redo();
    };

    struct AddAnnotationRule : public HistoryItem
    {
        AnnotationRule* LocalNode;
        Scene* Scene;

        AddAnnotationRule();
        ~AddAnnotationRule();

        virtual void undo();
        virtual void redo();
    };

    struct ChangeAnnotationRuleItem: public HistoryItem
    {
        Scene::AnnotationRuleChange Change;

        ChangeAnnotationRuleItem();
        ChangeAnnotationRuleItem(const Scene::AnnotationRuleChange& change);
        ~ChangeAnnotationRuleItem();

        virtual void undo();
        virtual void redo();
    };

    struct RemoveAnnotationRule: public HistoryItem
    {
        AnnotationRule* Rule; // A current rule
        Scene* Scene;

        QVector<AnnotationLine*> AttachedLines;

        RemoveAnnotationRule();
        ~RemoveAnnotationRule();

        virtual void undo();
        virtual void redo();
    };

    struct AddAnnotationLine: public HistoryItem
    {
        AnnotationLine* LocalAnnotationLine;
        Scene* Scene;

        AddAnnotationLine();
        ~AddAnnotationLine();

        virtual void undo();
        virtual void redo();
    };

    struct RemoveAnnotationLine: public HistoryItem
    {
        AnnotationLine* LocalAnnotationLine; // A current annotation line
        Scene* Scene;

        RemoveAnnotationLine();
        ~RemoveAnnotationLine();

        virtual void undo();
        virtual void redo();
    };

    struct AnnotationLineChange: public HistoryItem
    {
        AnnotationLine* LocalAnnotationLine;

        Maybe<unsigned int> OldFromValue;
        Maybe<unsigned int> NewFromValue;

        Maybe<unsigned int> OldToValue;
        Maybe<unsigned int> NewToValue;

        QLineF OldLine;
        QLineF NewLine;


        AnnotationLineChange();
        ~AnnotationLineChange();

        virtual void undo();
        virtual void redo();
    };
public:
    /*! Tries to load config from file
     *  \param[in] app
     *  \param[in] parent a parent data
     */
    MainWindow(QApplication* app, QWidget *parent = Q_NULLPTR);
    /*! Destroy history
     */
    ~MainWindow();
    /*! Tries to load config from file
     */
    void tryLoadConfigFromFile();
    /*! Tries to run parser
     *  \param[in] string input string
     */
    void tryRunParser(const QString& string);
    /*! Sets scenes for main window
     *  \param[in] source a source scene
     *  \param[in] dest a destination scene
     */
    void setScenes(Scene* source, Scene* dest);
    /*! Adds new history item to history
     *  \param[in] item an item description
     */
    void addHistoryItem(MainWindow::HistoryItem* item);
    /*! Sets destination scene for source
     *  \param source a source scene
     */
    void setDestinationScene(Scene* source);
    /*! Sets status label
     *  \param[in] s string
     */
    void setStatusLabel(const QString& s) const;
    /*! Handles key press event
     *  \param[in] e
     */
    virtual void keyPressEvent(QKeyEvent * e);
    /*! Sets editable name
     *  \param[in] name a name
     */
    void setEditableTextValue(const QString& name) const;
    /*! If node is selected, changes text
     * \param[in] node a node
     * \param[in] text a text
     */
    void tryChangeTextIfNodeIsSelected(Node* node, const QString& text) const;
public slots:
    /*! An actions for specifying folder for parser
     */
    void specifyFolderForParser(bool);
    /*! Stores config to file
     */
    void storeConfigToFile() const;
    /*! Tries to parse input, entered by user
     */
    void tryParse();
    /*! Copies source tree to destination
     */
    void copyToDestination();
    /*! An undo action for window
     */
    void undo();
    /*! A redo action for window
     */
    void redo();
    /*! An inner text is changed
     */
    void innerTextChanged() const;
    /*! Starts adding node
     */
    void startAddingNode() const;
    /*! Starts removing object
     */
    void startRemoving() const;
    /*! Starts adding arrow
     */
    void startAddingArrow() const;
    /*! Starts adding annotation rule
     */
    void startAddingAnnotationRule() const;
    /*! Starts adding annotation line
     */
    void startAddingAnnotationLine() const;
    /*! Disables editing node
     */
    void disableEditing() const;
    /*! Enables editing node
     */
    void enableEditing() const;
    /*! Creates new file in main window
     */
    void newFile();
    /*! Saves document in file, specified by user
     */
    void saveAs();
    /*! Saves document in file
     */
    void save();
    /*! Loads document from file
     */
    void load();
    /*! A new window
     */
    void newWindow() const;
private:
    /*! Writes parsed string to an input file of a parser
        \param[in] string a string data
     */
    bool writeParsedString(const QString& string) const;
    /*! Tries load parsed file to a string
     */
    QString tryReadParsedFile(bool* ok) const; 
    /*! Tries to read file to a string
     *  \param[in] file_name a name for file
     *  \param[out] ok a flag, whether ok
     *  \return file
     */
    static QString tryReadFile(const QString& file_name, bool* ok);
    /*! Parses main window
     *  \param[in] e element
     *  \param[out] storage a storage data
     *  \param[out] s scene to be filled
     *  \return created node
     */
    MainWindow::XMLNode* parse(
        const QDomElement& e,
        QHash<MainWindow::XMLNode*, ::Node*>& storage,
        Scene* s
    ) const;

    /*! Layout nodes in scene according to their placement
     *  \param[in] leftx a left part of node
     *  \param[in] topy a top node y
     *  \param[in] node a source node
     *  \param[in] storage a storage
     */
    static void layoutNodes(double leftx, double topy, MainWindow::XMLNode* node, const QHash<MainWindow::XMLNode*, ::Node*>& storage);
    /*! Recompute scene bounds, located in scene
     *  \param[in] scene a scene
     *  \param[in] storage a list of items
     */
    static void recomputeSceneBounds(Scene* scene, const QHash<MainWindow::XMLNode*, ::Node*>& storage);
    /*! Gets total width for a node and it's subtree
     *  \param[in] node a node
     *  \param[in] storage a storage data
     *  \return width
     */
    static double getWidthRecursive(MainWindow::XMLNode* node, const QHash<MainWindow::XMLNode*, ::Node*>& storage);
    /*! A ui object
     */
    Ui::MainWindow ui;
    /*! A parser path
     */
    QString m_parser_path;
    /*! A source scene data
     */
    Scene* m_source_scene;
    /*! A destination scene data
     */
    Scene* m_destination_scene;
    /*! A list of history items
     */
    QVector<MainWindow::HistoryItem*> m_history_items;
    /*! A history cursor position
     */
    int m_history_cursor_position;
    /*! An application
     */
    QApplication* m_app;
    /*! A stored file name
     */
    QString m_file_name;
};
