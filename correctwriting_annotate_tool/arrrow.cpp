#include "arrow.h"
#include "utils.h"

#include <QPainter>
#include <QtMath>

#define A_ROUNDING_PADDING 6
#define DEGREE 2

Arrow::Arrow() : Id(0)
{
    
}

QDomElement Arrow::toElement(QDomDocument& doc)
{
    QDomElement e = doc.createElement("Arrow");
    e.setAttribute("Id", toString(this->Id));

    e.setAttribute("P1", toString(this->line().p1()));
    e.setAttribute("P2", toString(this->line().p2()));
    e.setAttribute("FromId", toString(this->FromId));
    e.setAttribute("ToId", toString(this->ToId));

    return e;
}

bool Arrow::loadFromElement(QDomElement& e)
{
    Maybe<unsigned int> id;
    Maybe<Maybe<unsigned int> > from_id;
    Maybe<Maybe<unsigned int> > to_id;

    Maybe<QPointF> p1, p2;

    parse(e.attribute("Id"), id);
    parse(e.attribute("P1"), p1);
    parse(e.attribute("P2"), p2);
    parse(e.attribute("FromId"), from_id);
    parse(e.attribute("ToId"), to_id);

    if (id.exists && p1.exists && p2.exists && from_id.exists && to_id.exists)
    {
        this->Id = id.value;
        this->FromId = from_id.value;
        this->ToId = to_id.value;
        this->setLine(QLineF(p1.value, p2.value));
        return true;
    }

    return false;
}

void Arrow::paint(QPainter* p, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    this->QGraphicsLineItem::paint(p, option, widget);
    QPointF pivot = this->line().p2() - this->pos();
    double angle = this->line().angle();

    double bcos = cos(angle / 180.0*M_PI);
    double bsin = sin(angle / 180.0*M_PI);
    QPointF part = pivot - A_ROUNDING_PADDING * QPointF(bcos, -bsin);
    QPointF part2 = part + DEGREE * QPointF(bsin, bcos);
    QPointF part3 = part - DEGREE * QPointF(bsin, bcos);
    p->drawLine(pivot, part2);
    p->drawLine(pivot, part3);
}

 Arrow* Arrow::clone() const
 {
     Arrow* a = new Arrow();
     a->setLine(this->line());
     a->Id = this->Id;
     a->FromId = this->FromId;
     a->ToId = this->ToId;
     return a;
 }
