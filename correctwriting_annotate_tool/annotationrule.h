/*! An annotated rule text, that can be resized or not
 */
#pragma once
#include <QGraphicsItem>
#include <QString>
#include "utils.h"
#include <QDomElement>

/*! An annotation rule text
 */
class AnnotationRule: public QGraphicsItem
{
public:
    /*! Sets new annotation rule
     */
    AnnotationRule();
    /*! Sets text
     *  \param[in] text a text
     */
    void setText(const QString& text);
    /*! Returns a stored text in rule
     *  \return stored text
     */
    const QString& text() const;
    /*! Returns whether width is set
     */
    bool isSetMaximalWidth() const;
    /*! Return rectangle for rule
     *  \return rectangle
     */
    Maybe<QRectF> maximalRect() const;
    /*! Sets width a width
     *  \param[in] rectangle a rectangle
     */
    void setMaximalRect(const Maybe<QRectF>& rectangle);
    /*! Returns a bounding rectangle for rule
     *  \return rectangle
     */
    virtual QRectF boundingRect() const;
    /*! Clones a node
    */
    AnnotationRule* clone() const;
    /*! Returns an element, that stores an annotation rule
     *  \param[in] doc document
     *  \return element
     */
    QDomElement toElement(QDomDocument& doc);
    /*! Loads item from element
     *  \param[in] element
     *  \return result
     */
    bool loadFromElement(QDomElement& e);
    /*! An id for rule
     */
    unsigned int Id;
protected:
    /*! Paints an annotation
    */
    virtual void paint(QPainter* p, const QStyleOptionGraphicsItem* option, QWidget* widget);
private: 
    /*! Recompute all data
     */
    void recompute();
    /*! A stored text data
     */
    QString m_text;
    /*! A current rectangle
     */
    QRectF m_current_rect;
    /*! A maximal rectangle
     */
    Maybe<QRectF> m_maximal_rect;
};
