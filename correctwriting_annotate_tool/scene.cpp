#include "scene.h"
#include "node.h"
#include "arrow.h"
#include "utils.h"
#include "mainwindow.h"

#include <QGraphicsSceneMouseEvent>
#include <QPolygonF>
#include <QTimer>
#include <QScrollBar>

#define ARROW_SENSITIVITY_LIMIT 10

Scene::Scene(QObject* parent)
: QGraphicsScene(parent), 
m_max_id(0), 
m_can_move_arrows(true), 
m_main_window(NULL), 
m_current_node(NULL), 
m_current_arrow(NULL), 
m_first(false), 
m_current_rule(NULL), 
m_current_line(NULL),
m_last_clicked_on_node(true),
m_state(Scene::S_IDLE),
m_possible_state(Scene::S_IDLE)
{
    m_rule_resizing_states.insert(Scene::S_RESIZING_RULE_BOTTOM_LEFT_CORNER);
    m_rule_resizing_states.insert(Scene::S_RESIZING_RULE_BOTTOM_RIGHT_CORNER);
    m_rule_resizing_states.insert(Scene::S_RESIZING_RULE_TOP_LEFT_CORNER);
    m_rule_resizing_states.insert(Scene::S_RESIZING_RULE_TOP_RIGHT_CORNER);

    m_rule_resizing_states.insert(Scene::S_RESIZING_RULE_LEFT_SIDE);
    m_rule_resizing_states.insert(Scene::S_RESIZING_RULE_RIGHT_SIDE);
    m_rule_resizing_states.insert(Scene::S_RESIZING_RULE_TOP_SIDE);
    m_rule_resizing_states.insert(Scene::S_RESIZING_RULE_BOTTOM_SIDE);
}


unsigned int Scene::getId()
{
    unsigned int result = m_max_id;
    ++m_max_id;
    return result;
}

void Scene::addNode(Node* node)
{
    m_nodes.insert(node->Id, node);
}


void Scene::addArrow(Arrow* arrow)
{
    m_arrows.insert(arrow->Id, arrow);
}

void Scene::addAnnotationRule(AnnotationRule* rule)
{
    m_annotation_rules.insert(rule->Id, rule);
}

void Scene::addAnnotationLine(AnnotationLine* line)
{
    m_annotation_lines.insert(line->Id, line);
}

Scene* Scene::clone() const
{
    Scene* result = new Scene();
    result->m_max_id = this->m_max_id;
    result->setSceneRect(this->sceneRect());
    result->m_main_window = m_main_window;
    for(QHash<unsigned int, Node*>::const_iterator  it = m_nodes.begin(); it != m_nodes.end(); ++it)
    {
        Node* t = it.value()->clone();
        result->addNode(t);
        result->addItem(t);
    }

    for(QHash<unsigned int, Arrow*>::const_iterator  it = m_arrows.begin(); it != m_arrows.end(); ++it)
    {
        Arrow* t = it.value()->clone();
        result->addArrow(t);
        result->addItem(t);
    }

    for (QHash<unsigned int, AnnotationRule*>::const_iterator it = m_annotation_rules.begin(); it != m_annotation_rules.end(); ++it)
    {
        AnnotationRule* t = it.value()->clone();
        result->addAnnotationRule(t);
        result->addItem(t);
    }

    for (QHash<unsigned int, AnnotationLine*>::const_iterator it = m_annotation_lines.begin(); it != m_annotation_lines.end(); ++it)
    {
        AnnotationLine* t = it.value()->clone();
        result->addAnnotationLine(t);
        result->addItem(t);
    }

    return result;
}

void Scene::setWindow(MainWindow* win)
{
    m_main_window = win;
}

void Scene::mousePressEvent(QGraphicsSceneMouseEvent *e)
{
    this->QGraphicsScene::mousePressEvent(e);
    if ((m_state == Scene::S_IDLE) && m_can_move_arrows)
    {
         QPointF pos = e->scenePos();
         if (this->isClickedOnArrow(pos))
         {
             this->goIntoArrowChangingPointState();
             return;
         }
         if (this->isClickedOnAnnotationLine(pos))
         {
             this->goIntoAnnotationLineChangingPointState();
             return;
         }
    }
    if (m_state == Scene::S_IDLE)
    {
        QPointF pos = e->scenePos();
        if (this->isClickedOnNode(pos))
        {
            this->goToNodeMovingState(pos);
            return;
        }
        if (this->isClickedOnRuleCorners(pos))
        {
            this->goIntoRuleCornerResizeState(pos);
            return;
        }
        if (this->isClickedOnRuleSides(pos))
        {
            this->goIntoRuleSideResizeState(pos);
            return;
        }
        if (this->isClickedOnRule(pos))
        {
            this->goIntoRuleMovingState(pos);
            return;
        }
    }

    if (m_state == Scene::S_ADDING_NODE)
    {
        MainWindow::AddNode* item = new MainWindow::AddNode();
        item->LocalNode=  m_current_node;
        item->Scene = this;
        m_current_node->Id = this->getId();
        m_nodes.insert(m_current_node->Id, m_current_node);
        m_main_window->addHistoryItem(item);
        m_main_window->setStatusLabel("");
        m_state = Scene::S_IDLE;
        m_last_clicked_on_node = true;
    }

    if (m_state == Scene::S_REMOVING)
    {
        // Try find and erase node
        {
            QPointF pos = e->scenePos();
            for(QHash<unsigned int, Node*>::iterator it = m_nodes.begin(); it != m_nodes.end(); ++it)
            {
                Node* val = it.value();
                if (val->isVisible())
                {
                    if (isWithin(pos, val->sceneBoundingRect()))
                    {
                        // DO NOT RESET STATE, BECAUSE WE MIGHT RESUME REMOVAL
                        // m_state = Scene::S_IDLE;
                        // m_main_window->setStatusLabel("Removed...");
                        if (m_current_node == val)
                        {
                            m_current_node = NULL;
                        }

                        MainWindow::RemoveNode* item = new MainWindow::RemoveNode();
                        item->Node = val;
                        item->Scene = this;
                        item->AttachedFromArrows = this->getArrowsCommingFromNode(item->Node);
                        item->AttachedToArrows = this->getArrowsCommingToNode(item->Node);
                        item->AttachedLines = this->getLinesAttachedToNode(item->Node);
                        item->redo();

                        m_main_window->addHistoryItem(item);

                        return;
                    }
                }
            }
        }
        // Try find and erase arrow
        {
            QPointF scenePos = e->scenePos();
            bool found = false;
            Arrow* arrow = NULL;
            for(QHash<unsigned int, Arrow*>::iterator it = m_arrows.begin(); (it != m_arrows.end()) && (!found); ++it)
            {
                Arrow* a = it.value();
                if (a->isVisible())
                {
                    QPolygonF poly;
                    QLineF line = a->line();
                    poly.append(QPointF(line.p1() - QPointF(ARROW_SENSITIVITY_LIMIT, 0)));
                    poly.append(QPointF(line.p1() + QPointF(ARROW_SENSITIVITY_LIMIT, 0)));
                    poly.append(QPointF(line.p2() + QPointF(ARROW_SENSITIVITY_LIMIT, 0)));
                    poly.append(QPointF(line.p2() - QPointF(ARROW_SENSITIVITY_LIMIT, 0)));
                    if (poly.containsPoint(scenePos, Qt::OddEvenFill))
                    {
                        arrow = a;
                        found = true;
                    }
                }
            }
            if (found)
            {
                MainWindow::RemoveArrow* item = new MainWindow::RemoveArrow();
                item->Arrow = arrow;
                item->Scene = this;
                item->redo();
                m_main_window->addHistoryItem(item);
                // DO NOT RESET STATE, BECAUSE WE MIGHT RESUME REMOVAL
                // m_state = Scene::S_IDLE;
                // m_main_window->setStatusLabel("");
                return;
            }
        }
        // Try find and erase rule
        {
            QPointF pos = e->scenePos();
            if (this->isClickedOnRule(pos))
            {
                MainWindow::RemoveAnnotationRule* item = new MainWindow::RemoveAnnotationRule();
                item->Rule = m_current_rule;
                item->Scene = this;
                item->AttachedLines = this->getLinesAttachedToRule(m_current_rule);

                item->redo();

                m_main_window->addHistoryItem(item);
                // DO NOT RESET STATE, BECAUSE WE MIGHT RESUME REMOVAL
                // m_state = Scene::S_IDLE;
                // m_last_clicked_on_node = true;
                return;
            }
        }
        // Try find and erase annotation line
        {
            QPointF scenePos = e->scenePos();
            bool found = false;
            AnnotationLine* annotation_line = NULL;
            for(QHash<unsigned int, AnnotationLine*>::iterator it = m_annotation_lines.begin(); (it != m_annotation_lines.end()) && (!found); ++it)
            {
                AnnotationLine* a = it.value();
                if (a->isVisible())
                {
                    QPolygonF poly;
                    QLineF line = a->line();
                    poly.append(QPointF(line.p1() - QPointF(ARROW_SENSITIVITY_LIMIT, 0)));
                    poly.append(QPointF(line.p1() + QPointF(ARROW_SENSITIVITY_LIMIT, 0)));
                    poly.append(QPointF(line.p2() + QPointF(ARROW_SENSITIVITY_LIMIT, 0)));
                    poly.append(QPointF(line.p2() - QPointF(ARROW_SENSITIVITY_LIMIT, 0)));
                    if (poly.containsPoint(scenePos, Qt::OddEvenFill))
                    {
                        annotation_line = a;
                        found = true;
                    }
                }
            }
            if (found)
            {
                MainWindow::RemoveAnnotationLine* item = new MainWindow::RemoveAnnotationLine();
                item->LocalAnnotationLine = annotation_line;
                item->Scene = this;
                item->redo();
                m_main_window->addHistoryItem(item);
                // DO NOT RESET STATE, BECAUSE WE MIGHT RESUME REMOVAL
                // m_state = Scene::S_IDLE;
                // m_main_window->setStatusLabel("");
                return;
            }
        }
    }
    if (m_state == Scene::S_ARROW_PLACE_FIRST_POINT)
    {
        QPointF pos = e->scenePos();
        m_current_arrow = new Arrow();
        m_current_arrow->setLine(QLineF(pos, pos));
        this->addItem(m_current_arrow);
        m_state = Scene::S_ARROW_PLACE_SECOND_POINT;
        m_main_window->setStatusLabel("Click, where you want second point to be placed");
        return;
    }
    if (m_state == Scene::S_ARROW_PLACE_SECOND_POINT)
    {
        QLineF line = m_current_arrow->line();
        line.setP2(e->scenePos());
        m_current_arrow->setLine(line);
        m_current_arrow->Id = this->getId();

        bool found1 = false, found2 = false;
        for(QHash<unsigned int, Node*>::iterator it = m_nodes.begin(); (it != m_nodes.end()) && (!found1 || !found2); ++it)
        {
            Node* node = it.value();
            if (isWithin(m_current_arrow->line().p1(), node->sceneBoundingRect()) && !found1)
            {
                m_current_arrow->FromId  = {node->Id};
                QLineF line1 = m_current_arrow->line();
                line1.setP1(collisionPoint(line, node->sceneBoundingRect()).value);
                m_current_arrow->setLine(line1);
                found1 = true;
            }
            if (isWithin(m_current_arrow->line().p2(), node->sceneBoundingRect()) && !found2)
            {
                m_current_arrow->ToId  = {node->Id};
                QLineF line2 = m_current_arrow->line();
                line2.setP2(collisionPoint(line, node->sceneBoundingRect()).value);
                m_current_arrow->setLine(line2);
                found2 = true;
            }
        }

        m_arrows.insert(m_current_arrow->Id, m_current_arrow);

        MainWindow::AddArrow* item = new MainWindow::AddArrow();
        item->LocalArrow = m_current_arrow;
        item->Scene = this;
        m_main_window->addHistoryItem(item);



        m_state = Scene::S_IDLE;
        m_main_window->setStatusLabel("");
    }
    if (m_state == Scene::S_ADDING_RULE)
    {
        MainWindow::AddAnnotationRule* item = new MainWindow::AddAnnotationRule();
        item->LocalNode = m_current_rule;
        item->Scene = this;
        m_current_rule->Id = this->getId();
        m_annotation_rules.insert(m_current_rule->Id, m_current_rule);
        m_main_window->addHistoryItem(item);
        m_main_window->setStatusLabel("");
        m_state = Scene::S_IDLE;
        m_last_clicked_on_node = false;
    }
    if (m_state == Scene::S_ANNOTATION_LINE_PLACE_FIRST_POINT)
    {
        QPointF pos = e->scenePos();
        m_current_line = new AnnotationLine();
        m_current_line->setLine(QLineF(pos, pos));
        this->addItem(m_current_line);
        m_state = Scene::S_ANNOTATION_LINE_PLACE_SECOND_POINT;
        m_main_window->setStatusLabel("Click, where you want second point to be placed");
        return;
    }
    if (m_state == Scene::S_ANNOTATION_LINE_PLACE_SECOND_POINT)
    {
        this->mouseMoveEvent(e);

        QLineF line = m_current_line->line();
        bool first_point_could_be_on_rule = true; 
        if (this->isClickedOnNode(line.p1()))
        {
            line.setP1(collisionPoint(line, m_current_node->sceneBoundingRect()).value);
            m_current_line->FromId = {m_current_node->Id};
            first_point_could_be_on_rule = false;
        }
        else
        {
            m_current_line->FromId = Maybe<unsigned int>();
        }
        if (!first_point_could_be_on_rule) 
        {
            if (this->isClickedOnRule(line.p2()))
            {
                line.setP2(collisionPoint(line, m_current_rule->sceneBoundingRect()).value);
                m_current_line->ToId = {m_current_rule->Id};
            }
            else
            {
                m_current_line->ToId = Maybe<unsigned int>();
            }
        } 
        else
        {
            if (this->isClickedOnRule(line.p1()))
            {
                line =  QLineF(line.p2(), line.p1());

                line.setP2(collisionPoint(line, m_current_rule->sceneBoundingRect()).value);
                m_current_line->ToId = { m_current_rule->Id };

                if (this->isClickedOnNode(line.p1()))
                {
                    line.setP1(collisionPoint(line, m_current_node->sceneBoundingRect()).value);
                    m_current_line->FromId = { m_current_node->Id };
                }
                else
                {
                    m_current_line->FromId = Maybe<unsigned int>();
                }
            }
        }
        m_current_line->setLine(line);
        m_current_line->Id = this->getId();

        m_annotation_lines.insert(m_current_line->Id, m_current_line);

        MainWindow::AddAnnotationLine* item = new MainWindow::AddAnnotationLine();
        item->LocalAnnotationLine = m_current_line;
        item->Scene = this;
        m_main_window->addHistoryItem(item);

        m_state = Scene::S_IDLE;
        m_main_window->setStatusLabel("");
    }
}



void Scene::mouseReleaseEvent(QGraphicsSceneMouseEvent *e)
{
    this->QGraphicsScene::mouseReleaseEvent(e);
    if (m_state == Scene::S_MOVING_NODE)
    {
        moveNodeTo(e->scenePos());
        MainWindow::MoveChangeTextNode* item = new MainWindow::MoveChangeTextNode(m_change);
        m_main_window->addHistoryItem(item);
        m_main_window->setStatusLabel("");

        m_state = Scene::S_IDLE;
    }
    if (m_state == Scene::S_MOVING_ARROW)
    {
        moveArrowTo(e->scenePos());
        Maybe<unsigned int> new_value;
        bool found = false;
        for(QHash<unsigned int, Node*>::iterator it = m_nodes.begin(); (it != m_nodes.end()) && (!found); ++it)
        {
            Node* node = it.value();
            if (isWithin(e->scenePos(), node->sceneBoundingRect()))
            {
                new_value  = {node->Id};
                QLineF line = m_current_arrow->line();
                if (m_first)
                {
                    line.setP1(collisionPoint(line, node->sceneBoundingRect()).value);
                }
                else
                {
                    line.setP2(collisionPoint(line, node->sceneBoundingRect()).value);
                }
                m_current_arrow->setLine(line);
                found = true;
            }
        }
        Maybe<unsigned int> old_value = (m_first) ? m_current_arrow->FromId : m_current_arrow->ToId;
        if (m_first)
        {
            m_current_arrow->FromId = new_value;
        }
        else
        {
            m_current_arrow->ToId = new_value;
        }

        MainWindow::ArrowChange* item = new MainWindow::ArrowChange();
        item->LocalArrow = m_current_arrow;
        item->NewPoint = (m_first) ? m_current_arrow->line().p1() : m_current_arrow->line().p2();
        item->OldPoint = m_start_pos;
        item->OldValue = old_value;
        item->NewValue = new_value;
        m_main_window->addHistoryItem(item);
        m_main_window->setStatusLabel("");

        m_state = Scene::S_IDLE;
    }
    if (m_state == Scene::S_MOVING_RULE)
    {
        moveAnnotationRuleTo(e->scenePos());

        MainWindow::ChangeAnnotationRuleItem* item = new MainWindow::ChangeAnnotationRuleItem(m_rule_change);
        m_main_window->addHistoryItem(item);
        this->views()[0]->setCursor(QCursor(Qt::ArrowCursor));
        m_main_window->setStatusLabel("");

        m_state = Scene::S_IDLE;
    }
    if (m_rule_resizing_states.contains(m_state))
    {
        this->mouseMoveEvent(e);
        this->update();

        MainWindow::ChangeAnnotationRuleItem* item = new MainWindow::ChangeAnnotationRuleItem(m_rule_change);
        m_main_window->addHistoryItem(item);
        this->views()[0]->setCursor(QCursor(Qt::ArrowCursor));
        m_main_window->setStatusLabel("");

        m_state = Scene::S_IDLE;
    }
    if (m_state == Scene::S_MOVING_ANNOTATION_LINE)
    {
        moveAnnotationLineTo(e->scenePos());

        Maybe<unsigned int> old_from_value = m_current_line->FromId;
        Maybe<unsigned int> old_to_value = m_current_line->ToId;

        Maybe<unsigned int> new_from_value  = m_current_line->FromId;
        Maybe<unsigned int> new_to_value  = m_current_line->ToId;

        if (m_first)
        {
            QLineF line = m_current_line->line();
            if (this->isClickedOnNode(line.p1()))
            {
                 line.setP1(collisionPoint(line, m_current_node->sceneBoundingRect()).value);
                 new_from_value = {m_current_node->Id};
            }
            m_current_line->setLine(line);
            m_current_line->FromId=  new_from_value;
        }
        else
        {
            QLineF line = m_current_line->line();
            if (this->isClickedOnRule(line.p2()))
            {
                 line.setP2(collisionPoint(line, m_current_rule->sceneBoundingRect()).value);
                 new_to_value = {m_current_rule->Id};
            }
            m_current_line->setLine(line);
            m_current_line->ToId=  new_to_value;
        }

        MainWindow::AnnotationLineChange* item = new MainWindow::AnnotationLineChange();
        item->LocalAnnotationLine = m_current_line;

        item->OldLine = m_old_line;
        item->NewLine = m_current_line->line();

        item->OldFromValue = old_from_value;
        item->OldToValue = old_to_value;

        item->NewFromValue = new_from_value;
        item->NewToValue = new_to_value;

        m_main_window->addHistoryItem(item);
        m_main_window->setStatusLabel("");

        m_state = Scene::S_IDLE;
    }
}

void Scene::mouseMoveEvent(QGraphicsSceneMouseEvent *e)
{
    this->QGraphicsScene::mouseMoveEvent(e);
    if (m_state == Scene::S_MOVING_NODE)
    {
        moveNodeTo(e->scenePos());
        this->expandSceneRect(m_current_node);
    }
    if (m_state == Scene::S_MOVING_ARROW)
    {
        moveArrowTo(e->scenePos());
        this->expandSceneRect(m_current_arrow);
    }
    if (m_state == Scene::S_ADDING_NODE)
    {
        m_current_node->setPos(e->scenePos() - QPointF(m_current_node->sceneBoundingRect().width() / 2.0, m_current_node->sceneBoundingRect().height() / 2.0));
        this->expandSceneRect(m_current_node);
    }
    if (m_state == Scene::S_ARROW_PLACE_SECOND_POINT)
    {
        QLineF line = m_current_arrow->line();
        line.setP2(e->scenePos());
        m_current_arrow->setLine(line);
    }
    if (m_state == Scene::S_ADDING_RULE)
    {
        m_current_rule->setPos(e->scenePos() - QPointF(m_current_rule->sceneBoundingRect().width() / 2.0, m_current_rule->sceneBoundingRect().height() / 2.0));
        this->expandSceneRect(m_current_rule);
    }
    if (m_state == Scene::S_MOVING_RULE)
    {
        moveAnnotationRuleTo(e->scenePos());
        this->expandSceneRect(m_current_rule);
    }
    if (m_rule_resizing_states.contains(m_state))
    {
        QPointF pos = e->scenePos();
        resizeRuleAccordingToState(pos);
        this->expandSceneRect(m_current_rule);
        this->update();
    }
    if (m_state == Scene::S_ANNOTATION_LINE_PLACE_SECOND_POINT)
    {
        QLineF line = m_current_line->line();
        line.setP2(e->scenePos());
        this->expandSceneRect(m_current_line);
        m_current_line->setLine(line);
    }
    if (m_state == Scene::S_MOVING_ANNOTATION_LINE)
    {
        moveAnnotationLineTo(e->scenePos());
        this->expandSceneRect(m_current_line);
    }
}

void Scene::enableArrowMoving()
{
    m_can_move_arrows = true;
}

void Scene::disableArrowMoving()
{
    m_can_move_arrows = false;
}

void Scene::hideAndRemoveNode(Node* node)
{
    node->setVisible(false);
    this->update();
    if (m_current_node == node)
    {
        m_current_node = NULL;
    }
    m_nodes.remove(node->Id);
}

void Scene::showAndAddNode(Node* node)
{
    node->setVisible(true);
    this->update();
    m_nodes.insert(node->Id, node);
}

void Scene::hideAndRemoveArrow(Arrow* arrow)
{
    arrow->setVisible(false);
    this->update();
    if (m_current_arrow == arrow)
    {
        m_current_arrow = NULL;
    }
    m_arrows.remove(arrow->Id);
}

void Scene::showAndAddArrow(Arrow* arrow)
{
    arrow->setVisible(true);
    this->update();
    m_arrows.insert(arrow->Id, arrow);
}

void Scene::hideAndRemoveAnnotationRule(AnnotationRule* rule)
{
    rule->setVisible(false);
    this->update();
    if (m_current_rule == rule)
    {
        m_current_rule = NULL;
    }
    m_annotation_rules.remove(rule->Id);
}

void Scene::showAndAddAnnotationRule(AnnotationRule* rule)
{
    rule->setVisible(true);
    this->update();
    m_annotation_rules.insert(rule->Id, rule);
}

void Scene::hideAndRemoveAnnotationLine(AnnotationLine* line)
{
    line->setVisible(false);
    this->update();
    if (m_current_line == line)
    {
        m_current_line = NULL;
    }
    m_annotation_lines.remove(line->Id);
}

void Scene::showAndAddAnnotationLine(AnnotationLine* line)
{
    line->setVisible(true);
    this->update();
    m_annotation_lines.insert(line->Id, line);
}


void Scene::startAddingNode(const QString& node)
{
    if (m_state == Scene::S_IDLE)
    {
        QString nd = node.trimmed();
        if (nd.length() == 0)
        {
            nd = "Node";
        }
        m_current_node = new Node();
        m_current_node->setText(nd);
        this->addItem(m_current_node);

        m_current_node->setPos(300, 200);
        m_state = Scene::S_ADDING_NODE;
        this->m_main_window->setStatusLabel("Click where you want to place node or edit it\'s label");
    }
}

void Scene::textEdited(const QString& text)
{
    if (m_state == Scene::S_ADDING_NODE)
    {
        QString t = text.trimmed();
        if (t.length() == 0)
        {
            t = "Node";
        }
        m_current_node->setText(t);
        m_current_node->update();
    }
    if (m_state == Scene::S_ADDING_RULE)
    {
        QString t = text.trimmed();
        if (t.length() == 0)
        {
            t = "Node";
        }
        m_current_rule->setText(t);
        m_current_rule->update();
    }
    if (m_state == Scene::S_IDLE)
    {
        if (m_last_clicked_on_node && (m_current_node != NULL))
        {
            m_change = this->storeOldNodeInfo(m_current_node);
            QString t = text.trimmed();

            if (t.length() == 0)
            {
                t = "Node";
            }
            m_current_node->setText(t);
            this->propagateChangesInRectangleToArrowsAndLinesForNode(m_change, m_current_node);

            MainWindow::MoveChangeTextNode* item = new MainWindow::MoveChangeTextNode(m_change);
            m_main_window->addHistoryItem(item);
        }
        else
        {
            if (m_current_rule != NULL)
            {
                m_rule_change = this->storeOldRuleInfo(m_current_rule);
                QString t = text.trimmed();

                if (t.length() == 0)
                {
                    t = "Node";
                }
                m_current_rule->setText(t);
                this->update();
                this->propagateChangesInRectangleToLines(m_rule_change, m_current_rule);

                MainWindow::ChangeAnnotationRuleItem* item = new MainWindow::ChangeAnnotationRuleItem(m_rule_change);
                m_main_window->addHistoryItem(item);
            }
        }
    }
    if (m_state == Scene::S_MOVING_NODE)
    {
        if (m_last_clicked_on_node && (m_current_node != NULL))
        {
            QString t = text.trimmed();

            if (t.length() == 0)
            {
                t = "Node";
            }
            m_current_node->setText(t);
            this->propagateChangesInRectangleToArrowsAndLinesForNode(m_change, m_current_node);
        }
    }
    if ((m_state == Scene::S_MOVING_RULE) || m_rule_resizing_states.contains(m_state))
    {
        QString t = text.trimmed();
        if (t.length() == 0)
        {
            t = "Node";
        }
        m_current_rule->setText(t);
        m_current_rule->update();
    }
}

void Scene::escapeAction()
{
    if (m_state == Scene::S_ADDING_NODE)
    {
        this->removeItem(m_current_node);
        m_current_node = NULL;
        m_state = Scene::S_IDLE;
        this->m_main_window->setStatusLabel("Cancelled");
    }
    if (m_state == Scene::S_REMOVING)
    {
        m_state = Scene::S_IDLE;
        this->m_main_window->setStatusLabel("Cancelled");
    }
    if (m_state == Scene::S_ARROW_PLACE_FIRST_POINT)
    {
        m_state = Scene::S_IDLE;
        this->m_main_window->setStatusLabel("Cancelled");
    }
    if (m_state == Scene::S_ARROW_PLACE_SECOND_POINT)
    {
        m_state = Scene::S_IDLE;
        this->removeItem(m_current_arrow);
        m_current_arrow = NULL;
        this->m_main_window->setStatusLabel("Cancelled");
    }
    if (m_state == Scene::S_ADDING_RULE)
    {
        this->removeItem(m_current_rule);
        m_current_rule = NULL;
        m_state = Scene::S_IDLE;
        this->m_main_window->setStatusLabel("Cancelled");
    }
    if (m_state == Scene::S_ANNOTATION_LINE_PLACE_FIRST_POINT)
    {
        m_state = Scene::S_IDLE;
        this->m_main_window->setStatusLabel("Cancelled");
    }
    if (m_state == Scene::S_ANNOTATION_LINE_PLACE_SECOND_POINT)
    {
        m_state = Scene::S_IDLE;
        this->removeItem(m_current_line);
        this->update();
        m_current_line = NULL;
        this->m_main_window->setStatusLabel("Cancelled");
    }
}

void Scene::tryChangeTextIfNodeIsSelected(Node* node, const QString& text) const
{
    if (m_last_clicked_on_node)
    {
        if (m_current_node == node)
        {
            m_main_window->setEditableTextValue(text);
        }
    }
}

void Scene::startRemoving()
{
    m_state =  Scene::S_REMOVING;
    this->m_main_window->setStatusLabel("Please, click on object, which you want to remove");
}

void Scene::startAddingArrow()
{
    m_state = Scene::S_ARROW_PLACE_FIRST_POINT;
    this->m_main_window->setStatusLabel("Please, click, where you want to start arrow (node or empty space)");
}


void Scene::startAddingAnnotationRule(const QString& node)
{
    if (m_state == Scene::S_IDLE)
    {
        QString nd = node.trimmed();
        if (nd.length() == 0)
        {
            nd = "Node";
        }
        m_current_rule = new AnnotationRule();
        m_current_rule->setText(nd);
        this->addItem(m_current_rule);

        m_current_rule->setPos(300, 200);
        m_last_clicked_on_node = false;
        m_state = Scene::S_ADDING_RULE;
        this->m_main_window->setStatusLabel("Click where you want to place rule or edit it\'s label");
    }
}

void Scene::startAddngAnnotationLine()
{
    m_state = Scene::S_ANNOTATION_LINE_PLACE_FIRST_POINT;
    this->m_main_window->setStatusLabel("Click, where you want to start line (node or empty space)");
}

QDomElement Scene::toElement(QDomDocument& doc)
{
    QDomElement e = doc.createElement("Scene");
    e.setAttribute("MaxId", toString(this->m_max_id));
    e.setAttribute("SceneRect", toString(this->sceneRect()));
    for(QHash<unsigned int, Node*>::const_iterator  it = m_nodes.begin(); it != m_nodes.end(); ++it)
    {
        Node* t = it.value();
        if (t->isVisible())
        {
            e.appendChild(t->toElement(doc));
        }
    }

    for(QHash<unsigned int, Arrow*>::const_iterator  it = m_arrows.begin(); it != m_arrows.end(); ++it)
    {
        Arrow* t = it.value();
        if (t->isVisible())
        {
            e.appendChild(t->toElement(doc));
        }
    }

    for (QHash<unsigned int, AnnotationRule*>::const_iterator it = m_annotation_rules.begin(); it != m_annotation_rules.end(); ++it)
    {
        AnnotationRule* t = it.value();
        if (t->isVisible())
        {
            e.appendChild(t->toElement(doc));
        }
    }

    for (QHash<unsigned int, AnnotationLine*>::const_iterator it = m_annotation_lines.begin(); it != m_annotation_lines.end(); ++it)
    {
        AnnotationLine* t = it.value();
        if (t->isVisible())
        {
            e.appendChild(t->toElement(doc));
        }
    }
    return e;
}

bool Scene::loadFromElement(QDomElement& e)
{
    Maybe<QRectF> rect;
    Maybe<unsigned int> max_id;
    parse(e.attribute("SceneRect"), rect);
    parse(e.attribute("MaxId"), max_id);
    if (rect.exists && max_id.exists)
    {
        this->setSceneRect(rect.value);
        this->m_max_id = max_id.value;

        this->cleanup();

        bool error = false;
        QDomNode n = e.firstChild();
        while (!n.isNull() && !error)
        {
            QDomElement ce = n.toElement();  // Convert to element
            if (!ce.isNull())
            {
                bool handled = false;
                if (ce.tagName() == "Node")
                {
                    handled = true;
                    Node* node = new Node();
                    error = !(node->loadFromElement(ce));
                    if (error)
                    {
                        delete node;
                    }
                    else
                    {
                        this->addItem(node);
                        m_nodes.insert(node->Id, node);
                    }
                }
                if (ce.tagName() == "Arrow")
                {
                    handled = true;
                    Arrow* arrow = new Arrow();
                    error = !(arrow->loadFromElement(ce));
                    if (error)
                    {
                        delete arrow;
                    }
                    else
                    {
                        this->addItem(arrow);
                        m_arrows.insert(arrow->Id, arrow);
                    }
                }
                if (ce.tagName() == "AnnotationRule")
                {
                    handled = true;
                    AnnotationRule* rule = new AnnotationRule();
                    error = !(rule->loadFromElement(ce));
                    if (error)
                    {
                        delete rule;
                    }
                    else
                    {
                        this->addItem(rule);
                        m_annotation_rules.insert(rule->Id, rule);
                    }
                }
                if (ce.tagName() == "AnnotationLine")
                {
                    handled = true;
                    AnnotationLine* line = new AnnotationLine();
                    error = !(line->loadFromElement(ce));
                    if (error)
                    {
                        delete line;
                    }
                    else
                    {
                        this->addItem(line);
                        m_annotation_lines.insert(line->Id, line);
                    }
                }
                if (!handled)
                {
                    error = true;
                }
            }
            n = n.nextSibling();
        }
        return !error;
    }
    return false;
}

void Scene::cleanup()
{
    this->clear();

    for(QHash<unsigned int, Node*>::const_iterator  it = m_nodes.begin(); it != m_nodes.end(); ++it)
    {
        delete it.value();
    }

    for(QHash<unsigned int, Arrow*>::const_iterator  it = m_arrows.begin(); it != m_arrows.end(); ++it)
    {
        delete it.value();
    }

    for (QHash<unsigned int, AnnotationRule*>::const_iterator it = m_annotation_rules.begin(); it != m_annotation_rules.end(); ++it)
    {
        delete it.value();
    }

    for (QHash<unsigned int, AnnotationLine*>::const_iterator it = m_annotation_lines.begin(); it != m_annotation_lines.end(); ++it)
    {
        delete it.value();
    }

    m_nodes.clear();
    m_arrows.clear();
    m_annotation_rules.clear();
    m_annotation_lines.clear();
}

void Scene::expand()
{
    this->expandSceneRect();
}

void Scene::moveNodeTo(const QPointF& p)
{
    QPointF diff = p - m_clicked_pos;
    m_current_node->setPos(m_start_pos + diff);
    propagateChangesInRectangleToArrowsAndLinesForNode(m_change, m_current_node);
}

void Scene::moveArrowTo(const QPointF& p) const
{
    QPointF diff = p - m_clicked_pos;
    QLineF line = m_current_arrow->line();
    if (m_first)
    {
        line.setP1(m_start_pos + diff);
    }
    else
    {
        line.setP2(m_start_pos + diff);
    }
    m_current_arrow->setLine(line);
}

void Scene::moveAnnotationLineTo(const QPointF& p) const
{
    QPointF diff = p - m_clicked_pos;
    QLineF line = m_current_line->line();
    if (m_first)
    {
        line.setP1(m_start_pos + diff);
    }
    else
    {
        line.setP2(m_start_pos + diff);
    }
    m_current_line->setLine(line);
}

void Scene::moveAnnotationRuleTo(const QPointF& p)
{
    QPointF diff = p - m_clicked_pos;
    m_current_rule->setPos(m_start_pos + diff);
    this->propagateChangesInRectangleToLines(m_rule_change, m_current_rule);
}


QVector<Arrow*> Scene::getArrowsCommingFromNode(Node* node)
{
    QVector<Arrow*> result;
    for(QHash<unsigned int, Arrow*>::iterator cit = m_arrows.begin(); cit != m_arrows.end(); ++cit)
    {
        Arrow* a = cit.value();
        if (a->isVisible())
        {
            if (a->FromId == node->Id)
            {
                result << a;
            }
        }
    }
    return result;
}

QVector<Arrow*> Scene::getArrowsCommingToNode(Node* node)
{
    QVector<Arrow*> result;
    for(QHash<unsigned int, Arrow*>::iterator cit = m_arrows.begin(); cit != m_arrows.end(); ++cit)
    {
        Arrow* a = cit.value();
        if (a->isVisible())
        {
            if (a->ToId == node->Id)
            {
                result << a;
            }
        }
    }
    return result;
}

QVector<QPointF> Scene::getFirstPointsForArrows(const QVector<Arrow*>& arrows)
{
    QVector<QPointF> result;
    for (auto arrow : arrows)
    {
        result << arrow->line().p1();
    }
    return result;
}

QVector<QPointF> Scene::getLastPointsForArrows(const QVector<Arrow*>& arrows)
{
    QVector<QPointF> result;
    for (auto arrow : arrows)
    {
        result << arrow->line().p2();
    }
    return result;
}

QVector<AnnotationLine*> Scene::getLinesAttachedToNode(Node* node)
{
    QVector<AnnotationLine*> result;
    for(QHash<unsigned int, AnnotationLine*>::iterator cit = m_annotation_lines.begin(); cit != m_annotation_lines.end(); ++cit)
    {
        AnnotationLine* a = cit.value();
        if (a->isVisible())
        {
            if (a->FromId == node->Id)
            {
                result << a;
            }
        }
    }
    return result;
}

QVector<AnnotationLine*> Scene::getLinesAttachedToRule(AnnotationRule* rule)
{
    QVector<AnnotationLine*> result;
    for(QHash<unsigned int, AnnotationLine*>::iterator cit = m_annotation_lines.begin(); cit != m_annotation_lines.end(); ++cit)
    {
        AnnotationLine* a = cit.value();
        if (a->isVisible())
        {
            if (a->ToId == rule->Id)
            {
                result << a;
            }
        }
    }
    return result;
}

QVector<QPointF> Scene::getFirstPointsForLines(const QVector<AnnotationLine*>& lines)
{
    QVector<QPointF> result;
    for (auto line : lines)
    {
        result << line->line().p1();
    }
    return result;
}

QVector<QPointF> Scene::getLastPointsForLines(const QVector<AnnotationLine*>& lines)
{
    QVector<QPointF> result;
    for (auto line : lines)
    {
        result << line->line().p2();
    }
    return result;
}

void Scene::saveMovingResizingStateForRule(AnnotationRule* rule, const QPointF& p)
{
    m_current_rule = rule;
    m_clicked_pos = p;
    m_start_pos = rule->pos();

    m_rule_change = this->storeOldRuleInfo(rule);
    m_last_clicked_on_node = false;
    if (m_can_move_arrows)
    {
         m_main_window->setEditableTextValue(m_current_rule->text());
    }
}


bool Scene::isClickedOnRuleCorners(const QPointF& pos)
{
    for(QHash<unsigned int, AnnotationRule*>::iterator it = m_annotation_rules.begin(); it != m_annotation_rules.end(); ++it)
    {
        AnnotationRule* val = it.value();
        if (val->isVisible())
        {
            QRectF rect = val->sceneBoundingRect();
            QPointF points[4] = {rect.topLeft(), rect.topRight(), rect.bottomLeft(), rect.bottomRight()};
            Scene::State states[4] = {
                Scene::S_RESIZING_RULE_TOP_LEFT_CORNER,
                Scene::S_RESIZING_RULE_TOP_RIGHT_CORNER,
                Scene::S_RESIZING_RULE_BOTTOM_LEFT_CORNER,
                Scene::S_RESIZING_RULE_BOTTOM_RIGHT_CORNER
            };
            for(int i = 0; i < 4; i++)
            {
                QRectF local_rect(points[i].x() - ARROW_SENSITIVITY_LIMIT, points[i].y() - ARROW_SENSITIVITY_LIMIT, 2 * ARROW_SENSITIVITY_LIMIT, 2 * ARROW_SENSITIVITY_LIMIT);
                if (isWithin(pos, local_rect))
                {
                    m_possible_state = states[i];
                    m_current_rule = val;
                    return true;
                }
            }
        }
    }
    return false;
}

void Scene::goIntoRuleCornerResizeState(const QPointF& pos)
{
    Qt::CursorShape shapes[4] = {Qt::SizeFDiagCursor, Qt::SizeBDiagCursor,  Qt::SizeBDiagCursor, Qt::SizeFDiagCursor};
    Scene::State states[4] = {
        Scene::S_RESIZING_RULE_TOP_LEFT_CORNER,
        Scene::S_RESIZING_RULE_TOP_RIGHT_CORNER,
        Scene::S_RESIZING_RULE_BOTTOM_LEFT_CORNER,
        Scene::S_RESIZING_RULE_BOTTOM_RIGHT_CORNER
    };
    saveMovingResizingStateForRule(m_current_rule, pos);
    m_state = m_possible_state;
    this->m_main_window->setStatusLabel("Move the mouse to resize annotation rule");
    for(int i = 0; i < 4; i++) {
        if (states[i] == m_state) {
            this->views()[0]->setCursor(QCursor(shapes[i]));
        }
    }
}

bool Scene::isClickedOnRuleSides(const QPointF& pos)
{
    Scene::State states[4] = {
        Scene::S_RESIZING_RULE_LEFT_SIDE,
        Scene::S_RESIZING_RULE_RIGHT_SIDE,
        Scene::S_RESIZING_RULE_TOP_SIDE,
        Scene::S_RESIZING_RULE_BOTTOM_SIDE,
    };
    for(QHash<unsigned int, AnnotationRule*>::iterator it = m_annotation_rules.begin(); it != m_annotation_rules.end(); ++it)
    {
        AnnotationRule* val = it.value();
        if (val->isVisible())
        {
            QRectF rect = val->sceneBoundingRect();

            qreal left = rect.left();
            qreal right = rect.right();
            qreal top = rect.top();
            qreal bottom = rect.bottom();

            QRectF rects[4] = {
                QRectF(left - ARROW_SENSITIVITY_LIMIT / 2.0, top,  ARROW_SENSITIVITY_LIMIT, fabs(bottom - top)),  // NOLINT
                QRectF(right - ARROW_SENSITIVITY_LIMIT / 2.0, top, ARROW_SENSITIVITY_LIMIT, fabs(bottom - top)),  // NOLINT
                QRectF(left, top - ARROW_SENSITIVITY_LIMIT / 2.0, fabs(right - left),  ARROW_SENSITIVITY_LIMIT),  // NOLINT  
                QRectF(left, bottom - ARROW_SENSITIVITY_LIMIT / 2.0, fabs(right - left), ARROW_SENSITIVITY_LIMIT) // NOLINT
            };
            for(int i = 0; i < 4; i++)
            {
                if (isWithin(pos, rects[i]))
                {
                    m_possible_state = states[i];
                    m_current_rule = val;
                    return true;
                }
            }
        }
    }
    return false;
}


void Scene::goIntoRuleSideResizeState(const QPointF& pos)
{
    Qt::CursorShape shapes[4] = {Qt::SizeHorCursor, Qt::SizeHorCursor,  Qt::SizeVerCursor, Qt::SizeVerCursor};
    Scene::State states[4] = {
        Scene::S_RESIZING_RULE_LEFT_SIDE,
        Scene::S_RESIZING_RULE_RIGHT_SIDE,
        Scene::S_RESIZING_RULE_TOP_SIDE,
        Scene::S_RESIZING_RULE_BOTTOM_SIDE,
    };
    saveMovingResizingStateForRule(m_current_rule, pos);
    m_state = m_possible_state;
    this->m_main_window->setStatusLabel("Move the mouse to resize annotation rule");
    for(int i = 0; i < 4; i++) {
        if (states[i] == m_state) {
            this->views()[0]->setCursor(QCursor(shapes[i]));
        }
    }
}


bool Scene::isClickedOnRule(const QPointF& pos)
{
    for(QHash<unsigned int, AnnotationRule*>::iterator it = m_annotation_rules.begin(); it != m_annotation_rules.end(); ++it)
    {
        AnnotationRule* val = it.value();
        if (val->isVisible())
        {
            if (isWithin(pos, val->sceneBoundingRect()))
            {
                m_possible_state = S_MOVING_RULE;
                m_current_rule = val;
                return true;
            }
        }
    }
    return false;
}

void Scene::goIntoRuleMovingState(const QPointF& pos)
{
    saveMovingResizingStateForRule(m_current_rule, pos);
    m_state = m_possible_state;
    this->m_main_window->setStatusLabel("Move the mouse to move annotation rule");
    this->views()[0]->setCursor(QCursor(Qt::SizeAllCursor));
}


bool Scene::isClickedOnArrow(const QPointF& pos)
{
    for(QHash<unsigned int, Arrow*>::iterator it = m_arrows.begin(); it != m_arrows.end(); ++it)
    {
        Arrow* a = it.value();
        if (a->isVisible())
        {
            QLineF line = a->line();
            QRectF rect1(line.p1().x() - ARROW_SENSITIVITY_LIMIT, line.p1().y() - ARROW_SENSITIVITY_LIMIT, 2 * ARROW_SENSITIVITY_LIMIT, 2 * ARROW_SENSITIVITY_LIMIT);
            QRectF rect2(line.p2().x() - ARROW_SENSITIVITY_LIMIT, line.p2().y() - ARROW_SENSITIVITY_LIMIT, 2 * ARROW_SENSITIVITY_LIMIT, 2 * ARROW_SENSITIVITY_LIMIT);
            if (isWithin(pos, rect1))
            {
                m_start_pos = line.p1();
                m_clicked_pos = pos;
                m_current_arrow = a;
                m_first = true;
                m_possible_state = Scene::S_MOVING_ARROW;
                return true;
            }
            if (isWithin(pos, rect2))
            {
                m_start_pos = line.p2();
                m_clicked_pos = pos;
                m_current_arrow = a;
                m_first = false;
                m_possible_state = Scene::S_MOVING_ARROW;
                return true;
            }
        }
    }
    return false;
}

void Scene::goIntoArrowChangingPointState()
{
    m_state = m_possible_state;
    this->m_main_window->setStatusLabel("Move the cursor to position, where you want arrow point to be placed");
}


bool Scene::isClickedOnNode(const QPointF& pos)
{
    for(QHash<unsigned int, Node*>::iterator it = m_nodes.begin(); it != m_nodes.end(); ++it)
    {
        Node* val = it.value();
        if (val->isVisible())
        {
            if (isWithin(pos, val->sceneBoundingRect()))
            {
                m_possible_state = Scene::S_MOVING_NODE;
                m_current_node = val;

                return true;
            }
        }
    }
    return false;
}


void Scene::goToNodeMovingState(const QPointF& pos)
{
    m_state = m_possible_state;
    m_clicked_pos = pos;
    m_start_pos = m_current_node->pos();
    m_state = m_possible_state;
    m_last_clicked_on_node = true;
    m_change = this->storeOldNodeInfo(m_current_node);
    if (m_can_move_arrows)
    {
         m_main_window->setEditableTextValue(m_current_node->text());
    }
    this->m_main_window->setStatusLabel("Move to position, where you want node to be placed");
}


bool Scene::isClickedOnAnnotationLine(const QPointF& pos)
{
    for(QHash<unsigned int, AnnotationLine*>::iterator it = m_annotation_lines.begin(); it != m_annotation_lines.end(); ++it)
    {
        AnnotationLine* a = it.value();
        if (a->isVisible())
        {
            QLineF line = a->line();
            QRectF rect1(line.p1().x() - ARROW_SENSITIVITY_LIMIT, line.p1().y() - ARROW_SENSITIVITY_LIMIT, 2 * ARROW_SENSITIVITY_LIMIT, 2 * ARROW_SENSITIVITY_LIMIT);
            QRectF rect2(line.p2().x() - ARROW_SENSITIVITY_LIMIT, line.p2().y() - ARROW_SENSITIVITY_LIMIT, 2 * ARROW_SENSITIVITY_LIMIT, 2 * ARROW_SENSITIVITY_LIMIT);
            if (isWithin(pos, rect1))
            {
                m_start_pos = line.p1();
                m_clicked_pos = pos;
                m_current_line = a;
                m_first = true;
                m_possible_state = Scene::S_MOVING_ANNOTATION_LINE;
                return true;
            }
            if (isWithin(pos, rect2))
            {
                m_start_pos = line.p2();
                m_clicked_pos = pos;
                m_current_line = a;
                m_first = false;
                m_possible_state = Scene::S_MOVING_ANNOTATION_LINE;
                return true;
            }
        }
    }
    return false;
}

void Scene::goIntoAnnotationLineChangingPointState()
{
    m_state = m_possible_state;
    m_old_line = m_current_line->line();
    this->m_main_window->setStatusLabel("Move the cursor to position, where you want line point to be placed");
}



Scene::NodeChange Scene::storeOldNodeInfo(Node* node)
{
    Scene::NodeChange result;
    result.Node = node;

    result.OldText  = node->text();
    result.OldPosition = node->sceneBoundingRect();

    result.ArrowsComingFromNode = this->getArrowsCommingFromNode(node);
    result.ArrowsComingFromNodeOldPositions = this->getFirstPointsForArrows(result.ArrowsComingFromNode);

    result.ArrowsComingToNode = this->getArrowsCommingToNode(node);
    result.ArrowsComingToNodeOldPositions = this->getLastPointsForArrows(result.ArrowsComingToNode);

    result.LinesComingFromNode = this->getLinesAttachedToNode(node);
    result.LinesComingFromNodeOldPositions = this->getFirstPointsForLines(result.LinesComingFromNode);

    return result;
}


Scene::AnnotationRuleChange Scene::storeOldRuleInfo(AnnotationRule* rule)
{
     Scene::AnnotationRuleChange result;

     result.Rule = rule;
     result.OldText = rule->text();
     result.OldRect = rule->sceneBoundingRect();
     result.OldMaxRect = rule->maximalRect();

     result.Lines = this->getLinesAttachedToRule(rule);
     result.OldPositions = this->getLastPointsForLines(result.Lines);

     return result;
}

void Scene::propagateChangesInRectangleToArrowsAndLinesForNode(Scene::NodeChange& change, Node* node) const
{
    QRectF new_rect = node->sceneBoundingRect();

    qreal old_left = change.OldPosition.left();
    qreal old_width = change.OldPosition.width();
    qreal old_top = change.OldPosition.top();
    qreal old_height = change.OldPosition.height();

    qreal new_left = new_rect.left();
    qreal new_width  = new_rect.width();
    qreal new_top = new_rect.top();
    qreal new_height  = new_rect.height();

    for(int i = 0; i < change.ArrowsComingFromNode.size(); i++)
    {
        Arrow* a  = change.ArrowsComingFromNode[i];

        QLineF line = a->line();
        QPointF p1 = line.p1();
        p1.setX(new_left + (change.ArrowsComingFromNodeOldPositions[i].x() - old_left) / old_width * new_width);
        p1.setY(new_top + (change.ArrowsComingFromNodeOldPositions[i].y() - old_top) / old_height * new_height);
        line.setP1(p1);
        a->setLine(line);
    }

    for(int i = 0; i < change.ArrowsComingToNode.size(); i++)
    {
        Arrow* a  = change.ArrowsComingToNode[i];

        QLineF line = a->line();
        QPointF p2 = line.p2();
        p2.setX(new_left + (change.ArrowsComingToNodeOldPositions[i].x() - old_left) / old_width * new_width);
        p2.setY(new_top + (change.ArrowsComingToNodeOldPositions[i].y() - old_top) / old_height * new_height);

        line.setP2(p2);
        a->setLine(line);
    }

    for(int i = 0; i < change.LinesComingFromNode.size(); i++)
    {
        AnnotationLine* a  = change.LinesComingFromNode[i];

        QLineF line = a->line();
        QPointF p1 = line.p1();
        p1.setX(new_left + (change.LinesComingFromNodeOldPositions[i].x() - old_left) / old_width * new_width);
        p1.setY(new_top + (change.LinesComingFromNodeOldPositions[i].y() - old_top) / old_height * new_height);
        line.setP1(p1);
        a->setLine(line);
    }

    change.NewPosition = new_rect;
    change.NewText = node->text();
    change.ArrowsComingFromNodeNewPositions = this->getFirstPointsForArrows(change.ArrowsComingFromNode);
    change.ArrowsComingToNodeNewPositions = this->getLastPointsForArrows(change.ArrowsComingFromNode);
    change.LinesComingFromNodeNewPositions = this->getFirstPointsForLines(change.LinesComingFromNode);
}

void Scene::propagateChangesInRectangleToLines(Scene::AnnotationRuleChange& change, AnnotationRule* rule) const
{
    QRectF new_rect = rule->sceneBoundingRect();

    qreal old_left = change.OldRect.left();
    qreal old_width = change.OldRect.width();
    qreal old_top = change.OldRect.top();
    qreal old_height = change.OldRect.height();

    qreal new_left = new_rect.left();
    qreal new_width  = new_rect.width();
    qreal new_top = new_rect.top();
    qreal new_height  = new_rect.height();

    for(int i = 0; i < change.Lines.size(); i++)
    {
        AnnotationLine* a  = change.Lines[i];

        QLineF line = a->line();
        QPointF p2 = line.p2();
        p2.setX(new_left + (change.OldPositions[i].x() - old_left) / old_width * new_width);
        p2.setY(new_top + (change.OldPositions[i].y() - old_top) / old_height * new_height);

        line.setP2(p2);
        a->setLine(line);
    }

    change.NewRect = new_rect;
    change.NewText = rule->text();
    change.NewMaxRect = rule->maximalRect();
    change.NewPositions = this->getLastPointsForLines(change.Lines);
}

void Scene::resizeRuleAccordingToState(const QPointF& pos)
{
    qreal left = m_rule_change.OldRect.left();
    qreal right = m_rule_change.OldRect.right();

    qreal top = m_rule_change.OldRect.top();
    qreal bottom = m_rule_change.OldRect.bottom();
    if (m_state == Scene::S_RESIZING_RULE_TOP_LEFT_CORNER
        || m_state == Scene::S_RESIZING_RULE_TOP_RIGHT_CORNER
        || m_state == Scene::S_RESIZING_RULE_TOP_SIDE)
    {
        if (pos.y() > bottom)
        {
            top = bottom;
            bottom = pos.y();
        }
        else
        {
            top = pos.y();
        }
    }
    if (m_state == Scene::S_RESIZING_RULE_BOTTOM_LEFT_CORNER
        || m_state == Scene::S_RESIZING_RULE_BOTTOM_RIGHT_CORNER
        || m_state == Scene::S_RESIZING_RULE_BOTTOM_SIDE)
    {
        if (pos.y() < top)
        {
            bottom = top;
            top = pos.y();
        }
        else
        {
            bottom = pos.y();
        }
    }
    if (m_state == Scene::S_RESIZING_RULE_TOP_LEFT_CORNER
        || m_state == Scene::S_RESIZING_RULE_BOTTOM_LEFT_CORNER
        || m_state == Scene::S_RESIZING_RULE_LEFT_SIDE)
    {
        if (pos.x() > right)
        {
            left = right;
            right = pos.x();
        }
        else
        {
            left = pos.x();
        }
    }
    if (m_state == Scene::S_RESIZING_RULE_TOP_RIGHT_CORNER
        || m_state == Scene::S_RESIZING_RULE_BOTTOM_RIGHT_CORNER
        || m_state == Scene::S_RESIZING_RULE_RIGHT_SIDE)
    {
        if (pos.x() < left)
        {
            right  = left;
            left = pos.x();
        }
        else
        {
            right = pos.x();
        }
    }
    m_current_rule->setMaximalRect({QRectF(left, top, right - left, bottom - top) });
    m_current_rule->setPos(left, top);
    propagateChangesInRectangleToLines(m_rule_change, m_current_rule);
}


inline void try_expand_rect(
    qreal& top,
    qreal& bottom,
    qreal& left,
    qreal& right,
    bool& changed,
    QGraphicsItem* item
)
{
    QRectF new_rect = item->sceneBoundingRect();
    const double padding = 40;
    qreal my_top = new_rect.top() - padding;
    qreal my_bottom = new_rect.bottom() + padding;
    qreal my_left = new_rect.left() - padding;
    qreal my_right = new_rect.right() + padding;

    if (top > my_top)
    {
        top = my_top;
        changed = true;
    }

    if (bottom < my_bottom)
    {
        bottom = my_bottom;
        changed = true;
    }

    if (left > my_left)
    {
        left = my_left;
        changed = true;
    }

    if (right < my_right)
    {
        right = my_right;
        changed = true;
    }
}

void Scene::expandSceneRect(QGraphicsItem* item)
{
    QRectF rect = this->sceneRect();
    qreal top = rect.top();
    qreal bottom = rect.bottom();
    qreal left = rect.left();
    qreal right = rect.right();

    bool changed = false;
    if (item)
    {
        try_expand_rect(top, bottom, left, right, changed, item);
    }
    else
    {
        for(QHash<unsigned int, Node*>::const_iterator  it = m_nodes.begin(); it != m_nodes.end(); ++it)
        {
            try_expand_rect(top, bottom, left, right, changed, it.value());
        }

        for(QHash<unsigned int, Arrow*>::const_iterator  it = m_arrows.begin(); it != m_arrows.end(); ++it)
        {
            try_expand_rect(top, bottom, left, right, changed, it.value());
        }

        for (QHash<unsigned int, AnnotationRule*>::const_iterator it = m_annotation_rules.begin(); it != m_annotation_rules.end(); ++it)
        {
            try_expand_rect(top, bottom, left, right, changed, it.value());
        }

        for (QHash<unsigned int, AnnotationLine*>::const_iterator it = m_annotation_lines.begin(); it != m_annotation_lines.end(); ++it)
        {
            try_expand_rect(top, bottom, left, right, changed, it.value());
        }
    }

    if (changed)
    {
        QGraphicsView* view = this->views()[0];
        qreal x = view->horizontalScrollBar()->value();
        qreal y = view->verticalScrollBar()->value();
        this->setSceneRect(QRectF(left, top, right - left, bottom - top));
        this->update();
        view->horizontalScrollBar()->setValue(x);
        view->verticalScrollBar()->setValue(y);
    }
}


// ======================= Annotation Rule Change =======================

::Scene::AnnotationRuleChange& ::Scene::AnnotationRuleChange::operator=(const AnnotationRuleChange& annotation_rule_change)
= default;
